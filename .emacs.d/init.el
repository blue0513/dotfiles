;;; package --- Summary:
;;; Commentary:
;;; Code:

;; GC timing
(setq gc-cons-threshold (* 128 1024 1024))

;; package managers
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives
	  '(("gnu" . "http://elpa.gnu.org/packages/")
            ("melpa" . "http://melpa.org/packages/")
            ("org" . "http://orgmode.org/elpa/")))
(package-initialize)

;; wakatime
(setq wakatime-api-key "f1596ddb-1cd1-4251-95fa-7d6acef9a693")
;; (global-wakatime-mode)

;; notification
(defvar notification-center-title "Emacs")
(defun notification-center (msg)
  (let ((tmpfile (make-temp-file "notification-center")))
   (with-temp-file tmpfile
     (insert
      (format "display notification \"%s\" with title \"%s\""
              msg notification-center-title)))
   (unless (zerop (call-process "osascript" tmpfile))
     (message "Failed: Call AppleScript"))
   (delete-file tmpfile)))
;; (notification-center "我が友「Emacs」") ;; example

;; zoom-mode
(global-unset-key (kbd "M-z"))
(global-set-key (kbd "M-z") 'zoom)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#272822" "#F92672" "#A6E22E" "#E6DB74" "#66D9EF" "#FD5FF0" "#A1EFE4" "#d1d1d1"])
 '(anzu-deactivate-region t)
 '(anzu-mode-lighter "")
 '(anzu-search-threshold 1000)
 '(avy-migemo-function-names
   (quote
    ((ivy--regex-ignore-order :around ivy--regex-ignore-order-migemo-around)
     (counsel-clj :around avy-migemo-disable-around)
     (counsel-grep :around counsel-grep-migemo-around)
     counsel-grep-function-migemo counsel-grep-occur-migemo
     (counsel-git-occur :around counsel-git-occur-migemo-around)
     (counsel-find-file-occur :around counsel-find-file-occur-migemo-around)
     swiper--add-overlays-migemo
     (swiper--re-builder :around swiper--re-builder-migemo-around)
     (ivy--regex :around ivy--regex-migemo-around)
     (ivy--regex-plus :around ivy--regex-plus-migemo-around)
     ivy--highlight-default-migemo ivy-occur-revert-buffer-migemo ivy-occur-press-migemo avy-migemo-goto-char avy-migemo-goto-char-2 avy-migemo-goto-char-in-line avy-migemo-goto-char-timer avy-migemo-goto-subword-1 avy-migemo-goto-word-1 avy-migemo-isearch avy-migemo-org-goto-heading-timer avy-migemo--overlay-at avy-migemo--overlay-at-full)))
 '(compilation-message-face (quote default))
 '(custom-safe-themes
   (quote
    ("8ed752276957903a270c797c4ab52931199806ccd9f0c3bb77f6f4b9e71b9272" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(fci-rule-color "#3C3D37")
 '(flycheck-disabled-checkers (quote (javascript-jshint javascript-jscs)))
 '(highlight-changes-colors (quote ("#FD5FF0" "#AE81FF")))
 '(highlight-tail-colors
   (quote
    (("#3C3D37" . 0)
     ("#679A01" . 20)
     ("#4BBEAE" . 30)
     ("#1DB4D0" . 50)
     ("#9A8F21" . 60)
     ("#A75B00" . 70)
     ("#F309DF" . 85)
     ("#3C3D37" . 100))))
 '(magit-diff-use-overlays nil)
 '(neo-vc-integration (quote (face)))
 '(package-selected-packages
   (quote
    (rubocopfmt js-comint flycheck-posframe sound-wav haskell-mode highlight-parentheses rvm right-click-context ruby-electric pomidor hl-block-mode company-tabnine package-lint indium counsel undo-propose posframe restclient annotate ov alert visual-regexp flycheck-ensime diffview company-box ivy-xref ivy-historian smart-jump eglot lsp-ui company-lsp lsp-mode wgrep persistent-scratch lispxmp smart-mode-line-atom-one-dark-theme ivy-explorer markdown-preview-eww markdown-preview-mode flymd pager web-search gited company-prescient ivy-prescient ns-auto-titlebar dockerfile-mode sql-indent git-messenger git-link color-identifiers-mode rspec-mode yari flyspell-correct-popup flyspell-correct-ivy auto-package-update shackle add-node-modules-path all-the-icons-dired dired-subtree csv-mode edit-server deadgrep quick-preview applescript-mode zoom-window rinari vlf imenu-list docker-tramp vimish-fold git-timemachine deft frames-only-mode scratch-palette auto-save-buffers-enhanced scratch-pop mode-line-bell maxframe hl-anything simpleclip clipmon goto-chg dimmer popup-kill-ring deferred esh-autosuggest browse-kill-ring bm helpful sml-modeline fill-column-indicator selected codic syntactic-close total-lines visual-fill-column volatile-highlights real-auto-save hl-todo hl-sexp pomodoro string-inflection flycheck-color-mode-line smart-hungry-delete wakatime-mode ac-html-csswatcher rubocop comment-dwim-2 mwim omni-scratch robe sudo-ext rjsx-mode dumb-diff nlinum-relative nlinum ac-inf-ruby ac-html embrace fzf 0blayout disable-mouse aggressive-indent smartrep multiple-cursors ac-cider avy-migemo migemo ac-php ztree zoom yascroll yaml-mode workgroups2 whitespace-cleanup-mode which-key web-mode vue-mode vagrant-tramp use-package undohist undo-tree twittering-mode terminal-here tabbar symbol-overlay switch-window sudo-edit sublimity stripe-buffer sr-speedbar spaceline smooth-scrolling smooth-scroll smex smart-mode-line-powerline-theme slack simple-call-tree shell-pop ripgrep redo+ recentf-ext rainbow-mode rainbow-delimiters popwin popup-switcher point-undo php-mode php-completion phi-search-dired pdf-tools paredit package-utils org-ac open-junk-file neotree monokai-theme minimap markdown-mode magit latex-preview-pane key-chord jumplist jump-tree json-mode jinja2-mode ivy-rich isearch-dabbrev isearch+ indent-guide imenu-anywhere icicles hydra hungry-delete hiwin highlight-symbol highlight-numbers highlight-indent-guides helm google-translate google-this golden-ratio go-mode git-gutter-fringe git-gutter-fringe+ fuzzy-match fuzzy fold-this fold-dwim focus flyspell-popup flyspell-lazy flycheck-tip flycheck-pos-tip flycheck-popup-tip flx-isearch flex-isearch find-file-in-project expand-region exec-path-from-shell esup ensime elscreen-buffer-group electric-operator dumb-jump dired-details+ dashboard csharp-mode color-theme-solarized color-theme-monokai cider beacon bash-completion auto-yasnippet auto-complete-auctex auctex-latexmk atomic-chrome anzu anything-exuberant-ctags all-the-icons-ivy ag ace-window ace-popup-menu ace-jump-mode ac-js2)))
 '(pos-tip-background-color "#FFFACE")
 '(pos-tip-foreground-color "#272822")
 '(send-mail-function (quote mailclient-send-it))
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#F92672")
     (40 . "#CF4F1F")
     (60 . "#C26C0F")
     (80 . "#E6DB74")
     (100 . "#AB8C00")
     (120 . "#A18F00")
     (140 . "#989200")
     (160 . "#8E9500")
     (180 . "#A6E22E")
     (200 . "#729A1E")
     (220 . "#609C3C")
     (240 . "#4E9D5B")
     (260 . "#3C9F79")
     (280 . "#A1EFE4")
     (300 . "#299BA6")
     (320 . "#2896B5")
     (340 . "#2790C3")
     (360 . "#66D9EF"))))
 '(vc-annotate-very-old-color nil)
 '(wakatime-cli-path "/usr/local/bin/wakatime")
 '(wakatime-python-bin nil)
 '(weechat-color-list
   (quote
    (unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#d1d1d1" "#F8F8F0")))
 '(zoom-ignored-buffer-name-regexps (quote ("^*NeoTree" ":home" "^*Ilist")))
 '(zoom-ignored-major-modes (quote (LaTeX-mode LaTeX-math-mode twittering-mod)))
 '(zoom-minibuffer-preserve-layout nil)
 '(zoom-size (quote (0.618 . 0.618))))

;; path
(add-to-list 'exec-path "/usr/local/bin")
(add-to-list 'auto-mode-alist '("\\zshrc\\'" . sh-mode))
(add-to-list 'auto-mode-alist '("\\bashrc\\'" . sh-mode))

;; 最大化して起動
;; (set-frame-parameter nil 'fullscreen 'maximized)

;; command key as super
(setq mac-command-modifier 'super)
;; option key as meta
(setq mac-option-modifier 'meta)
;; right commnad key as hyper
(setq mac-right-command-modifier 'hyper)

(defun my-smart-copy ()
  "If selected region, copy the region.
If not selected region, copy thing-at-point."
  (interactive)
  (cond
   ((region-active-p)
	 (progn
	   (copy-region-as-kill (region-beginning) (region-end))
	   (message "region copied!")))
   ((thing-at-point 'symbol)
    (progn
      (kill-new (thing-at-point 'symbol))
      (message "copied!: %s" (thing-at-point 'symbol))))
   (t
    (message "No symbol or region selected"))))

(add-to-list 'load-path "~/github/cool-copy.el")
(require 'cool-copy)
(setq cool-copy-show 'posframe)

;; Max OS Keybind
(global-set-key (kbd "s-z") 'undo)
(global-set-key (kbd "s-s") 'save-buffer)
(global-set-key [(super x)] 'kill-region)
(global-set-key [(super c)] 'cool-copy)
;; (global-set-key [(super c)] 'kill-ring-save)
(global-set-key [(super v)] 'my-yank-and-indent-it)
(global-set-key [(super w)]
                (lambda () (interactive) (delete-frame)))
(global-set-key [(super a)] 'mark-whole-buffer)
(global-set-key [(super n)] 'new-frame)

;; kill ring と macのclipboardを共有
;; https://qiita.com/yynozk/items/f5ccc2b027a9aaa13fe4
(setq select-enable-clipboard t)

;; clipboard to kill ring（画像まで入ってしまう）
;; http://blog.sushi.money/entry/20110924/1316853933
(add-to-list 'load-path "~/.emacs.d/clipboard-to-kill-ring")
(require 'clipboard-to-kill-ring)
(clipboard-to-kill-ring t)

;; kill ring max hist
(setq kill-ring-max 100)

;; textの色情報などを破棄
(defadvice kill-new (around my-kill-ring-disable-text-property activate)
        (let ((new (ad-get-arg 0)))
        (set-text-properties 0 (length new) nil new)
        ad-do-it))

;; browse kill ring
(setq browse-kill-ring-highlight-current-entry t)
(setq browse-kill-ring-display-duplicates nil)
(setq browse-kill-ring-highlight-inserted-item t)
(setq browse-kill-ring-show-preview t)
(setq browse-kill-ring-separator "-----")
(setq browse-kill-ring-show-preview t)
(setq browse-kill-ring-display-style 'separated)
(setq browse-kill-ring-separator-face '((t (:foreground "slate gray" :bold t))))
(global-set-key (kbd "M-v") 'counsel-yank-pop)

;; popup kill ring
;; (global-set-key (kbd "M-v") 'popup-kill-ring)
(setq popup-kill-ring-popup-width 60)
(setq popup-kill-ring-interactive-insert nil)

;; multi byte font
(setq inhibit-compacting-font-caches t)
(prefer-coding-system 'utf-8)
;;ターミナルの文字コード
(set-terminal-coding-system 'utf-8)
;;キーボードから入力される文字コード
(set-keyboard-coding-system 'utf-8)
;;ファイルのバッファのデフォルト文字コード
(set-buffer-file-coding-system 'utf-8)
;;バッファのプロセスの文字コード
(setq default-buffer-file-coding-system 'utf-8)
;;ファイルの文字コード
(setq file-name-coding-system 'utf-8)
;;新規作成ファイルの文字コード
(set-default-coding-systems 'utf-8)

;; go to end fo line or code
(global-set-key (kbd "C-a") 'mwim-beginning-of-line-or-code)
(global-set-key (kbd "C-e") 'mwim-end-of-line-or-code)

;; backspace
(global-set-key (kbd "C-h") 'delete-backward-char)

;; cursor setting
(set-cursor-color "gray")
(blink-cursor-mode t)

;; hungry delete
(require 'smart-hungry-delete)
(smart-hungry-delete-add-default-hooks)
(global-set-key (kbd "<backspace>") 'smart-hungry-delete-backward-char)
(global-set-key (kbd "C-d") 'smart-hungry-delete-forward-char)
(define-key prog-mode-map (kbd "C-h") 'smart-hungry-delete-backward-char)
(define-key prog-mode-map (kbd "<backspace>") 'smart-hungry-delete-backward-char)

;; delete-current-line
(defun delete-current-line ()
  "Delete (not kill) the current line."
  (interactive)
  (save-excursion
    (delete-region
     (progn (forward-visible-line 0) (point))
     (progn (forward-visible-line 1) (point)))))
(global-unset-key (kbd "s-k"))
(global-set-key (kbd "s-k") 'delete-current-line)

;; twitter mode
(setq twittering-status-format "%RT{%FACE[bold]{RT}} @%S %RT{(retweeted by %S)}\n%t\n\n----------------------\n")
(setq twittering-account-authorization 'authorized)
(setq twittering-oauth-access-token-alist
      '(("oauth_token" . "3159117649-EGOIyyXUxC3Jxb6krqgpkJFmQiRtbCDssWVKYD1")
       ("oauth_token_secret" . "VURhRbZFLtvwNodByxzo756bWqBG7EXsO8Z3NI7irV9CD")
       ("user_id" . "3159117649")
       ("screen_name" . "blue_1617")
       ("x_auth_expires" . "0")))
;; (setq twittering-icon-mode t)
;; (setq twittering-tinyurl-service 'goo.gl)
;; (setq twittering-use-master-password t)
;; (setq twittering-convert-fix-size 48)
(setq twittering-number-of-tweets-on-retrieval 20)
(setq twittering-timer-interval 1000)
;; (twittering-enable-unread-status-notifier)

;; company
(require 'company)
(global-company-mode) ; 全バッファで有効にする
(setq company-transformers '(company-sort-by-backend-importance)) ;; ソート順
(setq company-idle-delay 0) ; デフォルトは0.5
(setq company-minimum-prefix-length 0) ; デフォルトは4
(setq company-selection-wrap-around t) ; 候補の一番下でさらに下に行こうとすると一番上に戻る
(setq completion-ignore-case t)
(setq company-dabbrev-downcase nil)
(global-set-key (kbd "C-M-i") 'company-complete)
(define-key company-active-map (kbd "C-n") 'company-select-next) ;; C-n, C-pで補完候補を次/前の候補を選択
(define-key company-active-map (kbd "C-p") 'company-select-previous)
(define-key company-search-map (kbd "C-n") 'company-select-next)
(define-key company-search-map (kbd "C-p") 'company-select-previous)
(define-key company-active-map (kbd "C-s") 'company-filter-candidates) ;; C-sで絞り込む
(define-key company-active-map (kbd "C-i") 'company-complete-selection) ;; TABで候補を設定
(define-key company-active-map [tab] 'company-complete-selection) ;; TABで候補を設定
(define-key company-active-map (kbd "C-f") 'company-complete-selection) ;; C-fで候補を設定
(define-key emacs-lisp-mode-map (kbd "C-M-i") 'company-complete) ;; 各種メジャーモードでも C-M-iで company-modeの補完を使う

(defvar company-mode/enable-yas t
  "Enable yasnippet for all backends.")
(defun company-mode/backend-with-yas (backend)
  (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
      backend
    (append (if (consp backend) backend (list backend))
            '(:with company-yasnippet))))
;; (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))

(setq company-backends
      '((company-tabnine :with company-yasnippet)
        (company-bbdb :with company-yasnippet)
        (company-semantic :with company-yasnippet)
        (company-capf :with company-yasnippet)
        (company-files :with company-yasnippet)
        (company-keywords :with company-yasnippet)))

;; add number to the list items
(setq company-show-numbers t)

;; jump to the number item
;; (defun ora-company-number ()
;;   "Forward to `company-complete-number'.

;; Unless the number is potentially part of the candidate.
;; In that case, insert the number."
;;   (interactive)
;;   (let* ((k (this-command-keys))
;;          (re (concat "^" company-prefix k)))
;;     (if (cl-find-if (lambda (s) (string-match re s))
;;                     company-candidates)
;;         (self-insert-command 1)
;;       (company-complete-number (string-to-number k)))))

;; (let ((map company-active-map))
;;   (mapc
;;    (lambda (x)
;;      (define-key map (format "%d" x) 'ora-company-number))
;;    (number-sequence 0 9))
;;   (define-key map " " (lambda ()
;;                         (interactive)
;;                         (company-abort)
;;                         (self-insert-command 1)))
;;   (define-key map (kbd "<return>") nil))

;; company fuzzy
;; (with-eval-after-load 'company
  ;; (company-flx-mode +1))

;; ;; auto-complete
(require 'auto-complete)
(require 'auto-complete-config)
;; (require 'fuzzy) ;; heavy
;; (setq ac-use-fuzzy t)
;; (global-auto-complete-mode t)
;; (ac-config-default)
;; (setq ac-delay 0)
;; (setq ac-auto-show-menu 0.05)
;; (ac-set-trigger-key "TAB")
;; (setq ac-use-menu-map t)
;; (setq ac-menu-height 25)
;; (setq ac-auto-start 2)
;; (setq ac-ignore-case t)

;; ac-source
(setq-default ac-sources 'ac-source-words-in-same-mode-buffers)

;; font
(add-to-list 'default-frame-alist '(font . "ricty-10"))

;; font for page line break
;; (set-fontset-font "fontset-default"
;; 		  (cons page-break-lines-char page-break-lines-char)
;; 		  (face-attribute 'default :family))

;; white space delete
;; (global-whitespace-cleanup-mode t)
(add-hook 'prog-mode-hook 'whitespace-cleanup-mode)

;; color theme
(setq monokai-foreground "#d1d1d1")
(load-theme 'monokai t)

;; 非アクティブウィンドウの背景色を設定
;; (require 'hiwin)
;; (hiwin-activate)
;; (set-face-background 'hiwin-face "grey30")

;; 非アクティブウィンドウの背景色を設定
(when (require 'dimmer nil t)
  (setq dimmer-percent 0.6)
  (setq dimmer-exclusion-regexp "^ \\*ivy\\|^ \\*ruby-block-posframe\\|^ \\*cool-copy-posframe\\|^\\*[Hh]elm\\|^ \\*Minibuf\\|^ \\*Echo\\|^\\*Calendar\\|*Org\\|^ \\*ivy-posframe")
  (dimmer-mode t)
  (defun dimmer-off ()
    (dimmer-mode -1)
    (dimmer-process-all))
  (defun dimmer-on ()
    (dimmer-mode 1)
    (dimmer-process-all))

  (add-hook 'focus-out-hook #'dimmer-off)
  (add-hook 'focus-in-hook #'dimmer-on))

;; nlinum-mode
(global-nlinum-mode t)
(defun my-nlinum-mode-hook ()
  (when nlinum-mode
	(setq-local nlinum-format
		(concat "%" (number-to-string
				 ;; Guesstimate number of buffer lines.
				 (ceiling (log (max 1 (/ (buffer-size) 80)) 10)))
			"d "))))
(add-hook 'nlinum-mode-hook #'my-nlinum-mode-hook)

;; (global-linum-mode)
;; (setq linum-delay t)
;; (defadvice linum-schedule (around my-linum-schedule () activate)
;;   (run-with-idle-timer 0.2 nil #'linum-update-current))

;; highlight todo mode
;; (require 'hl-todo)
;; (setq hl-todo-keyword-faces
;;   '(("HOLD" . "red")
;;     ("TODO" . "red")
;;     ("NEXT" . "red")
;;     ("THEM" . "red")
;;     ("PROG" . "red")
;;     ("OKAY" . "red")
;;     ("DONT" . "red")
;;     ("FAIL" . "red")
;;     ("DONE" . "red")
;;     ("FIXME" . "red")
;;     ("XXX"   . "red")
;;     ("XXXX"  . "red")
;;     ("???"   . "red")
;;     ("NOTE"   . "red")
;;     ("NB"   . "red")))
;; (global-hl-todo-mode t)

;; Tab
(setq tab-width 4)

;; メニューバーを非表示
(menu-bar-mode 1)

;; タイトルにフルパス表示
(setq frame-title-format "%f")

;; ツールバーを非表示
(tool-bar-mode -1)

;; 対応する括弧をハイライト
(show-paren-mode 1)

;; highlight unique symbol
(add-to-list 'load-path "~/.emacs.d/emacs-highlight-unique-symbol")
(require 'highlight-unique-symbol)
;; (highlight-unique-symbol t)
(setq highlight-unique-symbol:interval 0.1)

;; volatile-highlights
(require 'volatile-highlights)
(volatile-highlights-mode t)

;; smart mode line
(defvar sml/no-confirm-load-theme t)
(defvar sml/theme 'dark)
;; (setq sml/theme 'atom-one-dark)
(defvar sml/shorten-directory -1)
(defvar sml/show-time nil)
(setq sml/position-percentage-format nil)
(sml/setup)

;; (require 'sml-modeline
;; (sml-modeline-mode t)

(require 'total-lines)
(global-total-lines-mode t)
(defun my-set-line-numbers ()
  (setq-default mode-line-front-space
		(append mode-line-front-space
			'((:eval (format " (%d) ●" (- total-lines 1)))))))
(add-hook 'after-init-hook 'my-set-line-numbers)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; 起動時設定 ;;;;;;;;;

;; スタートアップメッセージを表示させない
(setq inhibit-startup-message 1)

;; default scroll bar消去
(scroll-bar-mode -1)
(require 'yascroll)
(global-yascroll-bar-mode 1)
(setq my-yascroll-color "#474088")
(set-face-background 'yascroll:thumb-fringe my-yascroll-color)
(set-face-foreground 'yascroll:thumb-fringe my-yascroll-color)
(set-face-background 'yascroll:thumb-text-area my-yascroll-color)

;; scratchの初期メッセージ消去
(setq initial-scratch-message "")

;; scratch pop
;; (require 'scratch-palette)
;; (global-set-key (kbd "s-t") 'scratch-palette-popup)
;; (setq scratch-palette-directory "~/Dropbox/junk/palette/")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; 括弧設定 ;;;;;;;;;

;; pair closure
(electric-pair-mode 1)
;; (add-to-list 'electric-pair-pairs '(?' . ?')) ;; ''
(add-to-list 'electric-pair-pairs '(?{ . ?})) ;; {}
(add-to-list 'electric-pair-pairs '(?`. ?`)) ;; ``

;; rainbow delimiters
(add-to-list 'load-path "~/.emacs.d/rainbow-delimiters")
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
(add-hook 'js-mode-hook #'rainbow-delimiters-mode)
(add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)
(add-hook 'ruby-mode-hook #'rainbow-delimiters-mode)
(add-hook 'scala-mode-hook #'rainbow-delimiters-mode)
(add-hook 'cider-mode-hook #'rainbow-delimiters-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; モード設定 ;;;;;;;;;

;; scss-mode
(add-to-list 'auto-mode-alist '("\\.scss$" . scss-mode))

;; コンパイルは compass watchで行うので自動コンパイルをオフ
(defun scss-custom ()
  "Scss-mode-hook."
  (and
   (set (make-local-variable 'css-indent-offset) 2)
   (set (make-local-variable 'scss-compile-at-save) nil)
   )
  )
(add-hook 'scss-mode-hook
	  '(lambda() (scss-custom)))

;; flycheck
(require 'json)
(require 'flycheck)
;; warnのfringeを消す
;; (set-face-attribute 'flycheck-fringe-warning nil :foreground (face-attribute 'fringe :background ))
;; (setq flycheck-indication-mode 'right-fringe)
(setq flycheck-check-syntax-automatically '(save idle-change mode-enabled))
(setq flycheck-idle-change-delay 3)
(global-set-key (kbd "M-n") 'flycheck-next-error)
;; (global-set-key (kbd "M-p") 'flycheck-previous-error)
(defun flycheck-next-error-loop-advice (orig-fun &optional n reset)
  ; (message "flycheck-next-error called with args %S %S" n reset)
  (condition-case err
	  (apply orig-fun (list n reset))
	((user-error)
	 (let ((error-count (length flycheck-current-errors)))
	   (if (and
			(> error-count 0)                   ; There are errors so we can cycle.
			(equal (error-message-string err) "No more Flycheck errors"))
		   ;; We need to cycle.
		   (let* ((req-n (if (numberp n) n 1)) ; Requested displacement.
				  ; An universal argument is taken as reset, so shouldn't fail.
				  (curr-pos (if (> req-n 0) (- error-count 1) 0)) ; 0-indexed.
				  (next-pos (mod (+ curr-pos req-n) error-count))) ; next-pos must be 1-indexed
			 ; (message "error-count %S; req-n %S; curr-pos %S; next-pos %S" error-count req-n curr-pos next-pos)
			 ; orig-fun is flycheck-next-error (but without advise)
			 ; Argument to flycheck-next-error must be 1-based.
			 (apply orig-fun (list (+ 1 next-pos) 'reset)))
		 (signal (car err) (cdr err)))))))
(advice-add 'flycheck-next-error :around #'flycheck-next-error-loop-advice)

(require 'flycheck-color-mode-line)

(eval-after-load "flycheck"
  '(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

;; .js, .jsx を web-mode で開く
(add-to-list 'auto-mode-alist '("\\.js[x]?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl.html" . web-mode))

;; prettier, formatter
(require 'prettier-js)
(add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'web-mode-hook 'prettier-js-mode)

;; (defun enable-minor-mode (my-pair)
;;   "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
;;   (if (buffer-file-name)
;;       (if (string-match (car my-pair) buffer-file-name)
;; 	  (funcall (cdr my-pair)))))
;; (add-hook 'web-mode-hook #'(lambda ()
;;                             (enable-minor-mode
;;                              '("\\.jsx?\\'" . prettier-js-mode))))

;; prettier config
;; (setq prettier-js-args '(
;;   "--trailing-comma" "all"
;;   "--bracket-spacing" "true"
;;   "--print-width" "120"
;;   "--use-tabs" "false"
;;   "--single-quote" "true"
;;   "--semi" "false"
;; ))

;; 拡張子 .js でもJSX編集モードに
(defvar web-mode-content-types-alist
  '(("jsx" . "\\.js[x]?\\'")))

;; web-mode flycheck
(add-hook 'web-mode-hook
	  '(lambda ()
	     (add-to-list 'web-mode-comment-formats '("jsx" . "//" ))
	     (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil))
	     (flycheck-add-mode 'javascript-eslint 'web-mode)
	     (flycheck-mode)
	     (setq web-mode-attr-indent-offset nil)
	     (setq web-mode-markup-indent-offset 2)
	     (setq web-mode-css-indent-offset 2)
	     (setq web-mode-code-indent-offset 2)
	     (setq web-mode-sql-indent-offset 2)
	     (setq indent-tabs-mode nil)
	     (setq tab-width 2)))
(setq-default indent-tabs-mode nil)
;; (setq javascript-indent-level 2)

(eval-after-load 'js-mode
  '(add-hook 'js-mode-hook #'add-node-modules-path))
(eval-after-load 'web-mode
  '(add-hook 'web-mode-hook #'add-node-modules-path))

(defun eslint-fix-file ()
  (interactive)
  (call-process-shell-command
   (mapconcat 'shell-quote-argument
	      (list "eslint" "--fix" (buffer-file-name)) " ") nil 0))

(defun eslint-fix-file-and-revert ()
  (interactive)
  (eslint-fix-file)
  (revert-buffer t t))

;; rubocop
(require 'rubocop)
(add-hook 'ruby-mode-hook 'rubocop-mode)

;; inf-ruby
(global-set-key (kbd "H-r") 'ruby-send-block-and-go)
;; (add-to-list 'inf-ruby-implementations '("pry" . "pry"))
;; (setq inf-ruby-default-implementation "pry")

;; ruby-mode flycheck
(add-hook 'ruby-mode-hook
	  '(lambda ()
		 (setq flycheck-checker 'ruby-rubocop)
		 (flycheck-mode 1)))

;; rubocop flycheck setting
;; http://futurismo.biz/archives/2213
(flycheck-define-checker ruby-rubocop
   "A Ruby syntax and style checker using the RuboCop tool."
   :command ("rubocop" "--format" "emacs"
             (config-file "--config" flycheck-rubocoprc)
             source)
   :error-patterns
   ((warning line-start
             (file-name) ":" line ":" column ": " (or "C" "W") ": " (message)
             line-end)
    (error line-start
           (file-name) ":" line ":" column ": " (or "E" "F") ": " (message)
           line-end))
   :modes (ruby-mode motion-mode))

;; if you want to use `ruby-lint' too, should comment out this code
;; (flycheck-define-checker ruby-rubocop
;;   "A Ruby syntax and style checker using the RuboCop tool.

;; See URL `http://batsov.com/rubocop/'."
;;   :command ("rubocop" "--display-cop-names" "--format" "emacs"
;;             (config-file "--config" flycheck-rubocoprc)
;;             (option-flag "--lint" flycheck-rubocop-lint-only)
;;             ;; "--stdin" source-original
;; 	    source)
;;   ;; :standard-input t
;;   :error-patterns
;;   ((info line-start (file-name) ":" line ":" column ": C: "
;;          (optional (id (one-or-more (not (any ":")))) ": ") (message) line-end)
;;    (warning line-start (file-name) ":" line ":" column ": W: "
;;             (optional (id (one-or-more (not (any ":")))) ": ") (message)
;;             line-end)
;;    (error line-start (file-name) ":" line ":" column ": " (or "E" "F") ": "
;;           (optional (id (one-or-more (not (any ":")))) ": ") (message)
;;           line-end))
;;   :modes (enh-ruby-mode ruby-mode)
;;   :next-checkers ((warning . ruby-rubylint)))

;; robe
;; (autoload 'robe-mode "robe" "Code navigation, documentation lookup and completion for Ruby" t nil)
;; (autoload 'robe-ac-setup "robe-ac" "robe auto-complete" nil nil)
;; (add-hook 'robe-mode-hook 'robe-ac-setup)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;; マウス等動作設定 ;;;;;

;; bufferの最後でカーソルを動かそうとしても音をならなくする
(defun next-line (arg)
  (interactive "p")
  (condition-case nil
	  (line-move arg)
	(end-of-buffer)))

;; エラー音をならなくする
(setq ring-bell-function 'ignore)

;; mode line を flash
(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-background 'mode-line)))
          (set-face-background 'mode-line "purple4")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-background 'mode-line fg))
                               orig-fg))))

;; save時にmode line を光らせる
(add-hook 'after-save-hook
	  (lambda ()
	    (let ((orig-fg (face-background 'mode-line)))
	      (set-face-background 'mode-line "dark green")
	      (run-with-idle-timer 0.1 nil
				   (lambda (fg) (set-face-background 'mode-line fg))
				   orig-fg))))

;; if buffer is modified, change mode line background color
;; (defun my-test()
;;   (set-face-background 'mode-line "LightSkyBlue4"))

;; (defun my-test2()
;;   (set-face-background 'mode-line "black"))

;; (add-hook 'first-change-hook #'my-test)
;; (add-hook 'after-save-hook   #'my-test2)

;; mode line にメモを追加
(defun my-append-memo-to-modeline(&optional text)
  (interactive "sMemo:")
  (setq display-time-format
		(concat text
				" %H:%M "))
  (display-time-mode 1))

;; マウススクロールは1行ごとに
(setq mouse-wheel-scroll-amount '(1 ((shift) . 5)))

;; マウススクロールの加速をやめる
(setq mouse-wheel-progressive-speed nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;; ショートカットコマンド ;;;;;

;; 履歴
(defmacro with-suppressed-message (&rest body)
  "Suppress new messages temporarily in the echo area and the `*Messages*' buffer while BODY is evaluated."
  (declare (indent 0))
  (let ((message-log-max nil))
	`(with-temp-message (or (current-message) "") ,@body)))
(require 'recentf)
(setq recentf-save-file "~/.emacs.d/.recentf")
(setq recentf-max-saved-items 3000)            ;; recentf に保存するファイルの数
(setq recentf-exclude '(".recentf"))           ;; .recentf自体は含まない
(setq recentf-auto-cleanup 'never)             ;; 保存する内容を整理
(run-with-idle-timer 30 t '(lambda () (with-suppressed-message (recentf-save-list)))) ;
(require 'recentf-ext)

;; Comment out / Uncommenting
(global-set-key (kbd "s-/") 'comment-dwim-2)

;; 直前のバッファに戻る
(global-set-key (kbd "s-[") 'switch-to-prev-buffer)
;; (global-set-key (kbd "s-[") 'my-temp)

;; 次のバッファに進む
(global-set-key (kbd "s-]") 'switch-to-next-buffer)

;; redo+
;; (require 'redo+)
(define-key global-map [(super shift z)] 'redo)
(setq undo-no-redo t) ; 過去のundoがredoされないようにする
(setq undo-limit 600000)
(setq undo-strong-limit 900000)

;; undo history
(require 'undohist)
(undohist-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; 検索系 ;;;;;;;;;;;;

;; case insensitive
(setq case-fold-search t)

;; find-name-dired
(define-key global-map [(super i)] 'find-name-dired)

;; grep
(define-key global-map [(super shift o)] 'counsel-git-grep)
(define-key global-map [(super f)] 'counsel-rg)

;; rgrepのheader messageを消去
(defun delete-grep-header ()
  (save-excursion
	(with-current-buffer grep-last-buffer
	  (forward-line 5)
	  (narrow-to-region (point) (point-max)))))
(defadvice grep (after delete-grep-header activate) (delete-grep-header))
(defadvice rgrep (after delete-grep-header activate) (delete-grep-header))

;; popwin
;; (setq popwin:special-display-config '(("*Help* *compilation*")))
;; (require 'popwin)
;; (push '("*compilation*" :height 20 :position bottom) popwin:special-display-config)
;; (push '(compilation-mode :position bottom) popwin:special-display-config)
;; (display-buffer "*compilation*")

;; EmacsにFocusが当たっている際のFace
(defun my-out-focused-mode-line()
  (set-face-background 'mode-line "purple3"))

(defun my-in-focused-mode-line()
  (set-face-background 'mode-line "black"))

(add-hook 'focus-out-hook 'my-out-focused-mode-line)
(add-hook 'focus-in-hook 'my-in-focused-mode-line)

;; rgrep時などに，新規にwindowを立ち上げる
(setq special-display-buffer-names '("*Help*" "*compilation*" "*interpretation*" "*grep*" "*Messages*"))
(setq special-display-frame-alist
	  (append (list
		   '(width . 100)
		   '(height . 50)
		   )
		  special-display-frame-alist))

;; "grepバッファに切り替える"
(defun my-switch-grep-buffer()
  (interactive)
  (if (get-buffer "*grep*")
	  (pop-to-buffer "*grep*")
	(message "No grep buffer")))
;; (global-set-key (kbd "s-e") 'my-switch-grep-buffer)

;; find-diredでいらない情報を消す
;; (require 'dired-details)
;; (dired-details-install)
;; (setq dired-details-hidden-string "")
;; (setq dired-details-hide-link-targets nil)

(defadvice find-dired-sentinel (after dired-details (proc state) activate)
  "find-diredでもdired-detailsを使えるようにする"
  (ignore-errors
	(with-current-buffer (process-buffer proc)
	  (dired-details-activate))))

;; diredモードでストライプをつける
;; (add-hook 'dired-mode-hook 'stripe-listify-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; Git ;;;;;;;;;;;;;

;; git-gutter-fringe
(require 'git-gutter-fringe+)
;; (global-git-gutter-mode +1)

(global-git-gutter+-mode 1)
;; (global git-gutter)
(global-unset-key (kbd "C-x C-v"))
(global-set-key (kbd "C-x C-v") 'git-gutter+-show-hunk-inline-at-point)
(global-unset-key (kbd "C-x C-r"))
(global-set-key (kbd "C-x C-r") 'git-gutter+-revert-hunk)
(global-unset-key (kbd "C-x C-n"))
(global-set-key (kbd "C-x C-n") 'git-gutter+-next-hunk)
(global-unset-key (kbd "C-x C-p"))
(global-set-key (kbd "C-x C-p") 'git-gutter+-previous-hunk)
(fringe-helper-define 'git-gutter-fr+-added nil
  "........"
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "........")
(fringe-helper-define 'git-gutter-fr+-deleted nil
  "........"
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "..XXXX.."
  "........")

;; ファイル編集時に，bufferを再読込
(global-auto-revert-mode 1)
;; git branch 変更時に mode lineを更新
(setq auto-revert-check-vc-info t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Others ;;;;;;;;;;;

;; 別バッファで開かない
(defvar dired-dwim-target 0)

;; exit用のコマンドを書き換える
(global-unset-key (kbd "C-x C-c"))
(defalias 'exit 'save-buffers-kill-emacs)

;; syntax check
(add-hook 'after-init-hook #'global-flycheck-mode)
(eval-after-load 'flycheck
  '(custom-set-variables
	'(flycheck-disabled-checkers '(javascript-jshint javascript-jscs))))

;; flyspell
(require 'flyspell-lazy)
(flyspell-lazy-mode 1)
(add-hook 'prog-mode-hook 'flyspell-mode)
(global-set-key (kbd "<C-f2>") 'flyspell-mode)

;; (require 'flyspell-correct-ivy)
(require 'flyspell-correct-popup)
(define-key flyspell-mode-map (kbd "C-;") 'flyspell-correct-wrapper)
;; (add-hook 'flyspell-mode-hook #'flyspell-popup-auto-correct-mode)

(defun my-flyspell-save-word ()
  (interactive)
  (let ((current-location (point))
	(word (flyspell-get-word)))
	(when (consp word)
	  (flyspell-do-correct 'save nil (car word) current-location (cadr word) (caddr word) current-location))))
(global-set-key (kbd "C-:") 'my-flyspell-save-word)

;; https://minatech.net/gnupack%E3%81%AEemacs%E3%81%A7%E3%82%B9%E3%83%9A%E3%83%AB%E3%83%81%E3%82%A7%E3%83%83%E3%82%AF%E3%82%92%E8%A1%8C%E3%81%86/
(setq ispell-program-name "aspell"
      ;; force the English dictionary, support Camel Case spelling check (tested with aspell 0.6)
      ispell-extra-args
      '("--sug-mode=ultra" "--lang=en_US" "--run-together" "--run-together-limit=5" "--run-together-min=2"))

;; aspell
(setq-default ispell-program-name "aspell")
(eval-after-load "ispell"
 '(add-to-list 'ispell-skip-region-alist '("[^\000-\377]+")))

;; auto save kill
(setq make-backup-files nil)
(setq auto-save-default nil)

;; バックアップファイルを作らない
(setq backup-inhibited t)

;; 変更ファイルの番号つきバックアップ
(setq version-control nil)

;; 編集中ファイルのバックアップ
(setq auto-save-list-file-name nil)
(setq auto-save-list-file-prefix nil)

;; ロックファイルを生成しない
(setq create-lockfiles nil)

;; backup file location
(setq auto-save-file-name-transforms   '((".*" "~/.emacs/backup/" t)))

;; Esc Esc Escでwindowが閉じるのを防ぐ
(global-set-key (kbd "M-ESC ESC") 'keyboard-quit)

;; anzu
(require 'anzu)
(global-anzu-mode +1)
(set-face-attribute 'anzu-mode-line nil
			:foreground "yellow" :weight 'bold)

;; other window
(defun my-other-window()
  (interactive)
  (other-window 1)
  (zoom))
(global-set-key (kbd "<C-tab>") 'my-other-window)

;; split window
(global-unset-key (kbd "C-x 2"))
(defun my-split-window-below()
  (interactive)
  (split-window-below)
  (zoom))
(global-set-key (kbd "C-x 2") 'my-split-window-below)

;; split window
(defun my-split-window-right()
  (interactive)
  (split-window-right)
  (zoom))
(global-set-key (kbd "C-x 3") 'my-split-window-right)

;; ivy
;; (add-to-list 'load-path "~/github/swiper")
(require 'ivy)
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(global-set-key "\C-s" 'swiper)
;; (global-set-key (kbd "C-s") 'swiper-isearch)
(global-set-key (kbd "C-x C-x") 'counsel-M-x)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(define-key global-map [(super r)] 'counsel-recentf)
(setq ivy-height 30)
(defvar swiper-include-line-number-in-search t)
(setq ivy-extra-directories nil)
(defvar counsel-find-file-ignore-regexp (regexp-opt '("./" "../")))
(global-set-key (kbd "s-p") 'ivy-switch-buffer)
(setq ivy-re-builders-alist
	  '((t . ivy--regex-plus)))
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

;; find file project
(require 'find-file-in-project)
(global-set-key [(super shift i)] 'counsel-git)
;; (global-set-key [(super shift i)] 'find-file-in-project)
;; (global-set-key [(super shift i)] 'find-file-in-project-by-selected)

;; counsel osx app
(add-to-list 'load-path "~/.emacs.d/counsel-osx-app")
(require 'counsel-osx-app)
(global-set-key (kbd "H-SPC") 'counsel-osx-app)

;; 列番号の表示
(column-number-mode t)
;; (require 'fill-column-indicator)
;; (add-hook 'prog-mode-hook 'fci-mode)
;; (setq fci-rule-column 120)

;; ace jump
;; (global-set-key (kbd "C-j") 'ace-window)
(defvar aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))

;; junk mode
(defvar open-junk-file-format "~/Dropbox/junk/%m/%d/%Y-%m-%d.")
(global-set-key (kbd "C-x j") 'open-junk-file)

;; memo mode
(global-set-key (kbd "C-x k") (lambda() (interactive)(find-file "~/Dropbox/junk/memo.md")))

;; deft mode
(setq deft-extensions '("txt" "org" "md"))
(setq deft-directory "~/Dropbox/junk")
(setq deft-recursive t)

;; focus setting about split window
(defadvice split-window (after move-point-to-new-window activate)
  "Moves the point to the newly created window after splitting."
  (other-window 1))

;; undo tree
;; (require 'undo-tree)
;; (global-undo-tree-mode t)
;; (global-set-key (kbd "C-x u") 'undo-tree-visualize)

;; neo-tree
(setq neo-theme 'ascii)
(setq neo-persist-show t) ;; delete-other-window で neotree ウィンドウを消さない
(setq neo-smart-open t) ;; neotree ウィンドウを表示する毎に current file のあるディレクトリを表示する
(setq neo-smart-open t)
(global-set-key "\C-o" 'neotree-toggle)


;; fuzzy complete
(define-key ac-completing-map (kbd "<tab>") 'ac-complete)

;; expand region
(require 'expand-region)
;; (global-set-key (kbd "C-:") 'er/expand-region)
;; (global-set-key (kbd "C-;") 'er/contract-region)

;; string-inflection
(require 'string-inflection)
(global-set-key (kbd "C-c C-u") 'string-inflection-all-cycle)

;; region選択時に入力すると，置き換え
(delete-selection-mode t)

;; select-current-line
(defun select-current-line ()
  "Select the current line"
  (interactive)
  (end-of-line) ; move to end of line
  (set-mark (line-beginning-position)))
(global-set-key [(super :)] 'select-current-line)
(global-unset-key (kbd "C-u"))
(global-set-key (kbd "C-u") 'set-mark-command)

;; text size
(define-key global-map [(super +)] 'text-scale-increase)
(define-key global-map [(super -)] 'text-scale-decrease)

;; dash board
;; (dashboard-setup-startup-hook)
;; (setq dashboard-startup-banner "/Users/blue0513/Desktop/mito/mito.png")
;; (setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
;; (setq dashboard-items '((recents  . 30)
;; 			(bookmarks . 5)
;; 			(agenda . 5)))

;; page move
(global-set-key (kbd "C-v") 'scroll-up-command)
(global-set-key (kbd "C-r") 'scroll-down-command)
;; (global-set-key (kbd "C-v") 'pager-page-down)
;; (global-set-key (kbd "C-r") 'pager-page-up)

;; diminish
(require 'diminish)
(eval-after-load "volatile-highlights" '(diminish 'volatile-highlights-mode))
(eval-after-load "prettier-js" '(diminish 'prettier-js-mode "PrJS"))
(eval-after-load "wakatime-mode" '(diminish 'wakatime-mode))
(eval-after-load "company" '(diminish 'company-mode "Comp"))
(eval-after-load "flyspell" '(diminish 'flyspell-mode "FlyS"))
(eval-after-load "yasnippet" '(diminish 'yas-minor-mode "YS"))
(eval-after-load "undo-tree" '(diminish 'undo-tree-mode))
(eval-after-load "indent-guide" '(diminish 'indent-guide-mode))
(eval-after-load "git-gutter" '(diminish 'git-gutter-mode "GitG"))
(eval-after-load "git-gutter+" '(diminish 'git-gutter+-mode "GitG"))
(eval-after-load "auto-complete" '(diminish 'auto-complete-mode "AC"))
(eval-after-load "ivy" '(diminish 'ivy-mode))
(eval-after-load "beacon" '(diminish 'beacon-mode))
(eval-after-load "auto-revert" '(diminish 'auto-revert-mode))
(eval-after-load "zoom" '(diminish 'zoom-mode))
(eval-after-load "disable-mouse" '(diminish 'global-disable-mouse-mode))
(eval-after-load "aggressive-indent" '(diminish 'aggressive-indent-mode "AI"))
(eval-after-load "abbrev" '(diminish 'abbrev-mode))
(eval-after-load "white-space-cleanup-mode" '(diminish 'whitespace-cleanup-mode "WSC"))
(eval-after-load "symbol-overlay" '(diminish 'symbol-overlay-mode))
(eval-after-load "selected" '(diminish 'selected-minor-mode))
(eval-after-load "hl-anything" '(diminish 'hl-highlight-mode))
(eval-after-load "workgroups2" '(diminish 'workgroups-mode))
(eval-after-load "color-identifiers-mode" '(diminish 'color-identifiers-mode))
(eval-after-load "eldoc" '(diminish 'eldoc-mode))
(eval-after-load "company" '(diminish 'company-mode))
(eval-after-load "yasnippet" '(diminish 'yas-minor-mode))
(eval-after-load "phantom-inline-comment" '(diminish 'phantom-inline-comment-auto-restore-mode))
(eval-after-load "phantom-inline-comment" '(diminish 'phantom-inline-comment-auto-save-mode))
(eval-after-load "flycheck-flash-mode-line" '(diminish 'flycheck-flash-mode-line))
(eval-after-load "ivy-posframe" '(diminish 'ivy-posframe-mode))
(eval-after-load "point-history" '(diminish 'point-history-mode))
(eval-after-load "rinari" '(diminish 'rinari-minor-mode))
(eval-after-load "rubocop" '(diminish 'rubocop-mode))
(eval-after-load "git-gutter+" '(diminish 'git-gutter+-mode))

;; ;; google translate
(require 'google-translate)
(require 'google-translate-default-ui)
(require 'popwin)

;; google-translate
(defvar google-translate-english-chars "[:ascii:]"
  "これらの文字が含まれているときは英語とみなす")
(defun google-translate-enja-or-jaen (&optional string)
  "regionか現在位置の単語を翻訳する。C-u付きでquery指定も可能"
  (interactive)
  (setq string
	(cond ((stringp string) string)
		  (current-prefix-arg
		   (read-string "Google Translate: "))
		  ((use-region-p)
		   (buffer-substring (region-beginning) (region-end)))
		  (t
		   (thing-at-point 'word))))
  (let* ((asciip (string-match
		  (format "\\`[%s]+\\'" google-translate-english-chars)
		  string)))
	(run-at-time 0.1 nil 'deactivate-mark)
	(google-translate-translate
	 (if asciip "en" "ja")
	 (if asciip "ja" "en")
	 string)))
(push '("\*Google Translate\*" :noselect t :height 0.5 :stick t) popwin:special-display-config)
(global-set-key (kbd "C-x C-t") 'google-translate-enja-or-jaen)

;; yasnippet
(require 'yasnippet)
(add-to-list 'load-path "~/.emacs.d/plugins/yasnippet")
(setq yas-snippet-dirs
	  '("~/.emacs.d/yasnippets"
	"~/.emacs.d/mysnippets"))
(yas-global-mode 1)
(setq-default ac-sources (push 'ac-source-yasnippet ac-sources))

;; Latex
(defun ac-LaTeX-mode-setup () ; add ac-sources to default ac-sources
  (setq ac-sources
	(append '(ac-source-math-unicode ac-source-math-latex ac-source-latex-commands)
		ac-sources)))
(add-hook 'LaTeX-mode-hook 'ac-LaTeX-mode-setup)

;; minibuffer command text color
(set-face-foreground 'minibuffer-prompt "magenta3")

;; avy
(avy-setup-default)
;; (global-set-key (kbd "C-t") 'ace-jump-line-mode)
;; (global-set-key (kbd "C-;") 'avy-goto-word-1)
(global-unset-key (kbd "C-j"))
(global-set-key (kbd "C-j") 'avy-goto-word-1)
(setq avy-background t)
(setq avy-all-windows t)
(setq avy-timeout-seconds 0.5)
(setq avy-keys-alist
      `((avy-goto-word-1 . (?s ?e ?l ?a))
	(avy-goto-word-0 . (?s ?e ?l ?a))))

(defun add-keys-to-avy (prefix c &optional mode)
    (define-key global-map
      (read-kbd-macro (concat prefix (string c)))
      `(lambda ()
         (interactive)
         (funcall (if (eq ',mode 'word)
                      #'avy-goto-word-1
                    #'avy-goto-char) ,c))))
;; Assing key bindings for all characters
;; eg, A-s-x will activate (avy-goto-char ?x), ie, all occurrence of x
;; (loop for c from ?! to ?~ do (add-keys-to-avy "M-s-" c))
;; eg, M-s-x will activate (avy-goto-word-1 ?x), ie, all words starting with x
;; (loop for c from ?! to ?~ do (add-keys-to-avy "H-" c 'word))

;; ivy-rich
(require 'ivy-rich)
(ivy-set-display-transformer 'ivy-switch-buffer 'ivy-rich-switch-buffer-transformer)

;; flycheck
(with-eval-after-load 'flycheck
  (flycheck-popup-tip-mode))

;; (with-eval-after-load 'flycheck
;;   (require 'flycheck-posframe)
;;   (add-hook 'flycheck-mode-hook #'flycheck-posframe-mode))

;; dumb-jump
(setq dumb-jump-mode t)
(setq dumb-jump-selector 'ivy)
(setq dumb-jump-use-visible-window nil)
;; (define-key global-map [(super d)] 'dumb-jump-go)
;; (define-key global-map [(super shift d)] 'dumb-jump-quick-look)
;; (define-key global-map [(super shift d)] 'dumb-jump-back)

(require 'smart-jump)
(smart-jump-setup-default-registers)
(define-key global-map [(super d)] 'smart-jump-go)

;; point-undo (replaced by jump-back)
(add-to-list 'load-path "~/dotfiles/.emacs.d/point-undo")
(require 'point-undo)
;; (global-set-key "\C-l" 'point-undo)

;; path
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

;; key-chord
;; (require 'key-chord)
;; (setq key-chord-two-keys-delay 0.08)
;; (setq key-chord-one-key-delay 0.2)
;; (key-chord-mode 1)
;; (key-chord-define-global "jj" 'avy-goto-char-timer)
;; (key-chord-define-global "FF" 'avy-goto-word-1)
;; (key-chord-define-global "SS" 'shell-pop)
;; (key-chord-define-global "TT" 'terminal-here-launch)
;; (key-chord-define-global "CC" 'completion-at-point)

;; git-complete
;; (global-unset-key (kbd "C-c C-c"))
;; (global-set-key (kbd "C-c C-c") 'git-complete)
(add-to-list 'load-path "~/.emacs.d/git-complete")
(require 'git-complete)
(setq git-complete-enable-autopair t)
(setq git-complete-repeat-line-completion t)

;; stopwatch
(add-to-list 'load-path "~/.emacs.d/stopwatch")
(require 'stopwatch)

;; modeline-timer
(add-to-list 'load-path "~/.emacs.d/emacs-mode-line-timer")
(require 'mode-line-timer)

;; git-undo
;; (add-to-list 'load-path "~/.emacs.d/git-undo/git-undo-el")
;; (require 'git-undo)

;; bufferの強制再読込み
(defun revert-buffer-no-confirm (&optional force-reverting)
  "Interactive call to revert-buffer. Ignoring the auto-save
 file and not requesting for confirmation. When the current buffer
 is modified, the command refuses to revert it, unless you specify
 the optional argument: force-reverting to true."
  (interactive "P")
  ;;(message "force-reverting value is %s" force-reverting)
  (if (or force-reverting (not (buffer-modified-p)))
      (revert-buffer :ignore-auto :noconfirm)
    (error "The buffer has been modified")))

(global-set-key (kbd "M-r") 'revert-buffer-no-confirm)

;; emacsclient
(require 'server)
(unless (server-running-p)
  (server-start))
(global-set-key (kbd "C-x #") 'server-edit)

;; point-undo ext
(require 'ring)
(require 'edmacro)
(defvar-local jump-back!--marker-ring nil)
(defun jump-back!--ring-update ()
  (let ((marker (point-marker)))
	(unless jump-back!--marker-ring
	  (setq jump-back!--marker-ring (make-ring 30)))
	(ring-insert jump-back!--marker-ring marker)))
(run-with-idle-timer 1 t 'jump-back!--ring-update)
(defun jump-back! ()
  (interactive)
  (if (ring-empty-p jump-back!--marker-ring)
	  (error "No further undo information")
	(let ((marker (ring-ref jump-back!--marker-ring 0))
	  (repeat-key (vector last-input-event)))
	  (ring-remove jump-back!--marker-ring 0)
	  (if (= (point-marker) marker)
	  (jump-back!)
	(goto-char marker)
	(message "(Type %s to repeat)" (edmacro-format-keys repeat-key))
	(set-temporary-overlay-map
	 (let ((km (make-sparse-keymap)))
	   (define-key km repeat-key 'jump-back!)
	   km))))))
(global-set-key "\C-l" 'jump-back!)

;; goto-chg
(require 'goto-chg)
(global-set-key [(super l)] 'goto-last-change)
(global-set-key (kbd "M-l") 'goto-last-change-reverse)

;; beacon
(beacon-mode t)
(setq beacon-size 50)
(setq beacon-color "green")
(setq beacon-blink-when-point-moves-vertically 5)
(setq beacon-blink-when-point-moves-horizontally nil)

;; ruby-mode
(setq ruby-insert-encoding-magic-comment nil)

;; eshell
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(setq display-buffer-alist '(("\\`\\*e?shell" display-buffer-same-window)))
;; (require 'esh-autosuggest)
;; (add-hook 'eshell-mode-hook #'esh-autosuggest-mode)
(setq ivy-do-completion-in-region t) ; this is the default
(defun setup-eshell-ivy-completion ()
  (define-key [remap eshell-pcomplete] 'completion-at-point)
  ;; only if you want to use the minibuffer for completions instead of the
  ;; in-buffer interface
  (setq-local ivy-display-functions-alist
              (remq (assoc 'ivy-completion-in-region ivy-display-functions-alist)
                    ivy-display-functions-alist)))
(add-hook 'eshell-mode-hook #'setup-eshell-ivy-completion)

;; finder open
(defun finder-current-dir-open()
  (interactive)
  (shell-command "open ."))

(defun finder-open(dirname)
  (interactive "DDirectoryName:")
  (shell-command (concat "open " dirname)))

;; (global-set-key (kbd "M-f") 'finder-current-dir-open)

;; Latex
(require 'tex)
(TeX-global-PDF-mode t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
;; (add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(setq TeX-PDF-mode t)

;; compile
(defun desperately-compile ()
  "Traveling up the path, find a Makefile and `compile'."
  (interactive)
  (with-temp-buffer
	(while (and (not (file-exists-p "Makefile"))
		(not (equal "/" default-directory)))
	  (cd ".."))
	(when (file-exists-p "Makefile")
	  (compile "make -k"))))
(global-set-key (kbd "C-x w") 'desperately-compile)
(setq compilation-scroll-output t)

;; shell-pop
(setq shell-pop-shell-type '("eshell" "*eshell*" (lambda () (eshell))))

;; atomic-chrome
;; (require 'atomic-chrome)
;; (atomic-chrome-start-server)
;; (setq atomic-chrome-extension-type-list '(atomic-chrome))

;; terminal-here
(require 'terminal-here)
(setq terminal-here-terminal-command (list "open" "-a" "iTerm.app" "."))

;; symbol-overlay
(require 'symbol-overlay)
(add-hook 'prog-mode-hook #'symbol-overlay-mode)
(add-hook 'markdown-mode-hook #'symbol-overlay-mode)
(global-set-key (kbd "M-i") 'symbol-overlay-put)
(define-key symbol-overlay-map (kbd "p") 'symbol-overlay-jump-prev)
(define-key symbol-overlay-map (kbd "n") 'symbol-overlay-jump-next)
(define-key symbol-overlay-map (kbd "C-g") 'symbol-overlay-remove-all)

;; highlight
;; http://kitchingroup.cheme.cmu.edu/blog/2015/07/28/A-highlight-annotation-mode-for-Emacs-using-font-lock/
(defun highlight-region (beg end)
 (interactive "r")
 (set-text-properties
  beg end
  '(font-lock-face (:background "Blue")
                   highlighted t
                   help-echo "highlighted")))

(defun highlight-clear ()
  "Clear highlight at point."
  (interactive)
  (when (get-text-property (point) 'highlighted)
    (set-text-properties
     (next-single-property-change (point) 'highlighted)
     (previous-single-property-change (point) 'highlighted)
     nil)))

(defun highlight-cycle (beg end)
  (interactive "r")
  (if (get-text-property (point) 'highlighted)
    (set-text-properties
     (next-single-property-change (point) 'highlighted)
     (previous-single-property-change (point) 'highlighted)
     nil)
    (set-text-properties
     beg end
     '(font-lock-face (:background "purple4")
		      highlighted t
		      help-echo "highlighted")))) 
;; (global-set-key (kbd "H-M-i") 'highlight-cycle)

;; hl-anything
(hl-highlight-mode t)
(global-set-key (kbd "H-M-i") 'hl-highlight-thingatpt-local)

;; php-mode-hook
(require 'php-mode)
(add-hook 'php-mode-hook
	  (lambda ()
		(require 'php-completion)
		(php-completion-mode t)
		;; (define-key php-mode-map (kbd "C-o") 'phpcmp-complete)
		(make-local-variable 'ac-sources)
		(setq ac-sources '(
				   ac-source-words-in-same-mode-buffers
				   ac-source-php-completion
				   ac-source-filename
				   ))))
;; migemo
(require 'migemo)
(setq migemo-dictionary "/usr/local/share/migemo/utf-8/migemo-dict")
(setq migemo-command "cmigemo")
(setq migemo-options '("-q" "--emacs"))
(setq migemo-user-dictionary nil)
(setq migemo-coding-system 'utf-8)
(setq migemo-regex-dictionary nil)
(load-library "migemo")
(migemo-init)

;; migemo + swiper
;; (require 'avy-migemo)
;; (avy-migemo-mode 1)
;; (require 'avy-migemo-e.g.swiper)
;; (require 'avy-migemo-e.g.counsel)

;; auto-complete for cider
;; (require 'ac-cider)
;; (add-hook 'cider-mode-hook 'ac-flyspell-workaround)
;; (add-hook 'cider-mode-hook 'ac-cider-setup)
;; (add-hook 'cider-repl-mode-hook 'ac-cider-setup)
;; (eval-after-load "auto-complete"
;;   '(progn
;; 	 (add-to-list 'ac-modes 'cider-mode)
;; 	 (add-to-list 'ac-modes 'cider-repl-mode)))

;; cider-mode
;; (setq cider-repl-use-pretty-printing t)
;; (add-hook 'clojure-mode-hook 'cider-mode)
;; (setq cider-font-lock-dynamically '(macro core function var))
;; (setq cider-overlays-use-font-lock t)
;; (setq nrepl-log-messages t)
;; (defvar cider-repl-display-help-banner nil)

;; selected
(require 'selected)
(selected-global-mode 1)
(define-key selected-keymap (kbd "q") #'selected-off)
(define-key selected-keymap (kbd "c") #'my:codic)
(define-key selected-keymap (kbd "g") #'my:google-this)
(define-key selected-keymap (kbd "t") #'google-translate-enja-or-jaen)
(define-key selected-keymap (kbd "f") #'vimish-fold)

;; google this
(require 'google-this)
(with-eval-after-load "google-this"
    (defun my:google-this ()
      "検索確認をスキップして直接検索実行"
      (interactive)
      (google-this (current-word) t)))

;; codic
(require 'codic)
(with-eval-after-load "codic"
    (defun my:codic ()
      "検索確認をスキップして直接検索実行"
      (interactive)
      (codic (current-word))))

;; multiple-cursor
(require 'multiple-cursors)
(require 'hydra)
(global-unset-key "\C-t")
(defhydra multiple-cursors-hydra (global-map "C-t")
  "
	 ^Up^            ^Down^        ^Other^
----------------------------------------------
[_p_]   Next    [_n_]   Next    [_l_] Edit lines
[_P_]   Skip    [_N_]   Skip    [_a_] Mark all
[_M-p_] Unmark  [_M-n_] Unmark  [_r_] Mark by regexp
^ ^             ^ ^             [_q_] Quit
"
  ("l" mc/ebdit-lines :exit t)
  ("a" mc/mark-all-like-this :exit t)
  ("n" mc/mark-next-like-this)
  ("N" mc/skip-to-next-like-this)
  ("M-n" mc/unmark-next-like-this)
  ("p" mc/mark-previous-like-this)
  ("P" mc/skip-to-previous-like-this)
  ("M-p" mc/unmark-previous-like-this)
  ("r" mc/mark-all-in-region-regexp :exit t)
  ("q" nil))

(defhydra move-word-hydra (global-map "C-w")
  "
[_f_] Forward-word
[_b_] Backward-word
"
  ("f" forward-word)
  ("b" backward-word))

;; aggressive indent mode
;; (require 'aggressive-indent)
;; (global-aggressive-indent-mode 1)
;; (add-hook 'markdown-mode-hook
;;	  '(lambda ()
;;		 (aggressive-indent-mode -1)))

;; performance
(setq redisplay-dont-pause t)

;; mac status bar settings
(define-key global-map [menu-bar options] "") ;; Option
(define-key global-map [menu-bar edit] "") ;; Edit
(define-key global-map [menu-bar tools] "") ;; Tools
(define-key global-map [menu-bar help-menu] "") ;; Help
(define-key global-map [menu-bar file] "") ;; File
(define-key global-map [menu-bar buffer] "") ;; Buffers

;; add your status on status bar
;;(defvar my-menu-bar-menu (make-sparse-keymap "Mine"))
;;(define-key global-map [menu-bar my-menu] (cons "Mine" my-menu-bar-menu))

;; workgroups2
;; (require 'workgroups2)
;; (setq wg-mode-line-display-on nil)
;; (setq wg-session-load-on-start nil)
;; (workgroups-mode 1)

;; presentation mode
(add-to-list 'load-path "~/.emacs.d/emacs-presentation-mode")
(require 'presentation)

;; ;; cursor color change by input language
(defun mac-selected-keyboard-input-source-change-hook-func ()
  (set-cursor-color (if (string-match "com.apple.keylayout.ABC" (mac-input-source))
                        "#d1d1d1" "DeepPink3")))
(add-hook 'mac-selected-keyboard-input-source-change-hook
          'mac-selected-keyboard-input-source-change-hook-func)

;; magit: open current window
(setq magit-display-buffer-function
      (lambda (buffer)
        (display-buffer
         buffer (if (and (derived-mode-p 'magit-mode)
                         (memq (with-current-buffer buffer major-mode)
                               '(;;magit-process-mode
                                 ;; magit-revision-mode
				 magit-diff-mode
                                 ;;magit-stash-mode
                                 ;;magit-status-mode
				 )))
                    nil
                  '(display-buffer-same-window)))))

;; magit-diff
(global-set-key (kbd "M-m") 'magit-diff-range)

;; improve magit peformance
(setq magit-diff-highlight-indentation nil)
(setq magit-diff-highlight-trailing nil)
(setq magit-diff-paint-whitespace nil)
(setq magit-diff-highlight-hunk-body nil)
(setq magit-diff-refine-hunk nil)

;; magit face for file name
(eval-after-load 'magit
  '(set-face-foreground 'magit-diff-file-heading "cyan"))

(require 'vimish-fold)
(global-set-key (kbd "C-@") 'vimish-fold-delete)
(setq vimish-fold-indication-mode nil)

(define-key process-menu-mode-map (kbd "C-k") 'joaot/delete-process-at-point)

(defun joaot/delete-process-at-point ()
  (interactive)
  (let ((process (get-text-property (point) 'tabulated-list-id)))
    (cond ((and process
                (processp process))
           (delete-process process)
           (revert-buffer))
          (t
           (error "no process at point!")))))

;; For complex scala files
(setq max-lisp-eval-depth 50000)
(setq max-specpdl-size 5000)

;; ensime
(require 'ensime)
(require 'sbt-mode)
(setq sbt:scroll-to-bottom-on-output t)

;;https://github.com/shibayu36/emacs/blob/master/emacs.d/inits/20-edit-mode-scala.el
(add-hook 'scala-mode-hook 'ensime-scala-mode-hook)
(setq ensime-startup-snapshot-notification nil)
(setq ensime-eldoc-hints nil) ;; カーソル移動が重くなるのでやめる
(setq ensime-completion-style nil) ;; 一旦補完はなし
(setq ensime-typecheck-when-idle nil) ;; 定期的にtypecheckするのをやめる
(setq ensime-sem-high-enabled-p nil) ;; semantic highlightをしない
(require 'flycheck-ensime)
;;; 今のファイルのimportが書かれている部分にpopupする
(defun scala/popup-on-last-import ()
  (interactive)
  (popwin:popup-buffer (current-buffer) :height 0.4)
  (re-search-backward "^import " nil t))

(defun ensime-restart ()
  "Restart the ensime server."
  (interactive)
  (ensime-shutdown)
  (sit-for 2)
  (ensime))

(defun scala/enable-eldoc ()
  "Show error message or type name at point by Eldoc."
  (setq-local eldoc-documentation-function
              #'(lambda ()
                  (when (ensime-connected-p)
                    (let ((err (ensime-print-errors-at-point))) err))))
  (eldoc-mode +1))

;; (add-hook 'ensime-mode-hook #'scala/enable-eldoc)
(setq ensime-search-interface 'ivy)
(setq ensime-startup-notification nil)

(defun ensime-popup-type ()
  (interactive)
  (let* ((type (ensime-type-at-point-full-name)))
    (popup-tip type)))
(global-set-key (kbd "C-t") 'ensime-popup-type)

;; Test function for counsel
(defun my-counsel-rg()
    (interactive)
  (setq counsel-rg-base-command "rg -S --no-heading --line-number --color never %s .")
  (counsel-rg))

(defun my-counsel-rg2()
  (interactive)
  (setq counsel-rg-base-command "rg -S --no-heading --line-number --color never -C 2 %s .")
  (counsel-rg))

(global-set-key (kbd "s-f") 'my-counsel-rg)
(global-set-key (kbd "H-f") 'my-counsel-rg)

;; docker tramp
(require 'docker-tramp-compat)
(set-variable 'docker-tramp-use-names t)

;; imenu
(setq imenu-list-focus-after-activation t)
(setq imenu-list-size 70)
;; (setq imenu-list-position 'below)
;; (global-set-key (kbd "H-j") #'imenu-list-smart-toggle)

;; fold-this
(global-set-key (kbd "M-f") 'fold-this)

;; vc-annotateで現在の行がmergeされたPRを開く
(defun vc-annotate-open-pr-at-line ()
  (interactive)
  (let* ((rev-at-line (vc-annotate-extract-revision-at-line))
         (rev (car rev-at-line)))
    (shell-command (concat "pull-pr-from-commit " rev))))
;; (global-set-key (kbd "H-p") 'vc-annotate-open-pr-at-line)

(defun gitlab-open-mr ()
  (interactive)
  (let* ((rev-at-line (vc-annotate-extract-revision-at-line))
         (rev (car rev-at-line)))
    (shell-command (mapconcat 'shell-quote-argument
			      (list "gitlab-mr-from-commit" rev) " "))))
(global-set-key (kbd "H-o") 'gitlab-open-mr)

;; tramp mode
;; alias should be here
(defun tramp-to-wired ()
  (interactive)
  (find-file "/ssh:aoki@wired.cyber.t.u-tokyo.ac.jp#54322:/home/aoki/"))

(defun tramp-to-aoki ()
  (interactive)
  (find-file "/ssh:taijuaoki@taijuaoki.net#61203:/home/taijuaoki/"))

(setq tramp-verbose 6)

;; (defun tramp-to-aws ()
;;   (interactive)
;;   (find-file "/ssh:aoki@wired.cyber.t.u-tokyo.ac.jp#54322:/home/aoki/"))

;; view learge file
(require 'vlf-setup)
(add-hook 'vlf-mode-hook (lambda() (read-only-mode t)))

;; instant-maximized-window
(add-to-list 'load-path "~/.emacs.d/instant-maximized-window")
(require 'instant-maximized-window)
(global-set-key [(super return)] 'window-temp-maximize)

;; shohex version
;; (require 'zoom-window)
;; (global-set-key [(super return)] 'zoom-window-zoom)
;; (custom-set-variables
;;  '(zoom-window-mode-line-color "DarkGreen"))

;; rinari (for Rails)
(require 'rinari)
(add-hook 'ruby-mode-hook 'rinari-minor-mode)

;; yank 後にインデント
;; https://qiita.com/__hage/items/5159635b4912c1196ee3
(defun my-yank-and-indent-it ()
  "Yank and indentat it."
  (interactive)
  (yank)
  (save-excursion
    (exchange-point-and-mark)
    (indent-region (point) (mark))))

;; modelineのshow/hide
(set-default 'my-mode-line-format mode-line-format)
(defun my-mode-line-off ()
  "Turn off mode line."
  (setq my-mode-line-format mode-line-format)
  (setq mode-line-format nil))

(defun my-toggle-mode-line ()
  "Toggle mode line."
  (interactive)
  (when mode-line-format
    (setq my-mode-line-format mode-line-format))
  (if mode-line-format
      (setq mode-line-format nil)
    (setq mode-line-format my-mode-line-format)
    (redraw-display))
  (message "%s" (if mode-line-format "Modeline ON" "Modeline OFF!")))

(global-set-key (kbd "H-m") 'my-toggle-mode-line)
;; (add-hook 'find-file-hook #'my-mode-line-off)
;; ;; bufferのEOFを表示
;; (setq-default indicate-buffer-boundaries
;;               '((top . nil) (bottom . right) (down . right)))

;; (defun my-terminal-open()
;;   (interactive)
;;   (shell-command "iterm_control"))

;; (setq my-sound-path "~/Desktop/sound.m4a")
;; (defun my-open-sound()
;;   (interactive)
;;   (shell-command (concat "afplay " my-sound-path)))

;; (add-hook 'after-init-hook #'my-open-sound)

;; emojify
;; (add-hook 'after-init-hook #'global-emojify-mode)

;; alias
(defun my-twitter-post()
  (interactive)
  (let ((result (ignore-errors
		  (twittering-update-status-interactive)
		  t)))
    (unless result
      (twit))))
;; (global-set-key (kbd "C-t C-u") 'my-twitter-post)

(defun my-twitter-visit-timeline(list-name)
  (interactive "slist-name: ")
  (let ((result (ignore-errors
		  (twittering-visit-timeline (concat "blue_1617/" list-name))
		  t)))
    (unless result
      (twit))))
;; (global-set-key (kbd "C-t C-t") 'my-twitter-visit-timeline)

;; ChromeをReloadする
(defun my-reload-chrome()
  (interactive)
  (shell-command "reload_chrome"))
;; (global-set-key (kbd "H-r") 'my-reload-chrome)

;; ChromeのTabを切り替える
(defun my-switch-chrome-tab()
  (interactive)
  (shell-command "switch_chrome_tab"))
(global-set-key (kbd "H-t") 'my-switch-chrome-tab)

;; ChromeのTabを切り替える（左移動）
(defun my-switch-chrome-tab_prev()
  (interactive)
  (shell-command "switch_chrome_tab_prev"))
(global-set-key (kbd "H-T") 'my-switch-chrome-tab_prev)

;; Chromeを上下にスクロールする
(defun my-scroll-chrome-down()
  (interactive)
  (shell-command "scroll_chrome next"))
(global-set-key (kbd "H-j") 'my-scroll-chrome-down)

(defun my-scroll-chrome-up()
  (interactive)
  (shell-command "scroll_chrome previous"))
(global-set-key (kbd "H-k") 'my-scroll-chrome-up)

(require 'quick-preview)
;; (define-key dired-mode-map (kbd "SPC") 'quick-preview-at-point)
;; (define-key counsel-find-file-map (kbd "SPC") 'quick-preview-at-point)

(defun my-vimium()
  (interactive)
  (call-process-shell-command
   "~/shellscript/chrome_vimium/chrome_vimium&"
   nil 0))
;; (global-set-key (kbd "H-e") 'my-vimium)

(defun my-vimium-input(my-input)
  (interactive "sString:")
  (call-process-shell-command
   (concat "~/shellscript/chrome_vimium/chrome_vimium_input " my-input)
   nil 0))
;; (global-set-key (kbd "H-w") 'my-vimium-input)

;; for json format
(defun jq-format (beg end)
  (interactive "r")
  (shell-command-on-region beg end "jq ." nil t))

;; ;; copy current path to clipboard
;; (defun my-get-curernt-path ()
;;   (if (equal major-mode 'dired-mode)
;;       default-directory
;;     (buffer-file-name)))
;; (defun my-copy-current-path ()
;;   (interactive)
;;   (let ((fPath (my-get-curernt-path)))
;;     (when fPath
;;       (message "stored path: %s" fPath)
;;       (kill-new (file-truename fPath)))))

;;;;;;;;;;;
;; Scrapbox
;;;;;;;;;;;

;; Create New Page or Insert to Last Row
(defun my-post-scrapbox(beg end string)
  (interactive "r\nsPageName: ")
  (if mark-active
      (progn
	(let* ((body-string (buffer-substring-no-properties beg end))
	       (page-name string))
	(browse-url
	 (concat "https://scrapbox.io/aokitaiju0513-55403904/" page-name
		 "?body=" body-string))))
    (message "No Region Selected")))

;; Show a Page Body
(defun my-get-scrapbox (page-name)
  (interactive "sPageName:")
  (async-shell-command
   (concat "~/shellscript/scrapbox/get_scrapbox_page.sh " page-name)))

;; Show Pages List
(defun my-index-scrapbox ()
  (interactive)
  (async-shell-command "~/shellscript/scrapbox/get_scrapbox_page_titles.sh"))

;; deadgrep
(global-set-key (kbd "<f5>") 'deadgrep)

(defun my-post-twitter(beg end)
  (interactive "r")
  (if mark-active
      (progn
	(let* ((body-string (buffer-substring-no-properties beg end)))
	  (shell-command (concat "python ~/Python/announce_for_me/main.py " body-string))))
    (message "No Region Selected")))

;; dired-subtree
(define-key dired-mode-map "i" 'dired-subtree-toggle)

(add-to-list 'load-path "~/.emacs.d/icy-tail")
(require 'icy-tail)

(add-to-list 'load-path "~/github/rspec-on-iterm")
(require 'rspec-on-iterm)

(add-to-list 'load-path "~/.emacs.d/gitlab-mr-check")
(require 'gitlab-mr-check)

(defun my-check-code-in-github(beg end)
  (interactive "r")
  (if mark-active
      (progn
	(let* ((body-string (buffer-substring-no-properties beg end)))
	  (shell-command
	   (concat "ruby ~/ruby/works/search_code_in_github/github_api.rb " body-string))))
    (message "No Region Selected")))
(global-set-key (kbd "M-s") 'my-check-code-in-github)

(defun my-calc(formula)
  (interactive "sFormula: ")
  (shell-command (concat "echo " "\"" formula "\"" "| bc -q")))

(add-to-list 'load-path "~/github/how-many")
(require 'how-many)
(global-set-key (kbd "H-i") 'how-many)

;; https://zck.me/emacs-move-file
(defun move-file (new-location)
  "Write this file to NEW-LOCATION, and delete the old one."
  (interactive (list (expand-file-name
                      (if buffer-file-name
                          (read-file-name "Move file to: ")
                        (read-file-name "Move file to: "
                                        default-directory
                                        (expand-file-name (file-name-nondirectory (buffer-name))
                                                          default-directory))))))
  (when (file-exists-p new-location)
    (delete-file new-location))
  (let ((old-location (expand-file-name (buffer-file-name))))
    (message "old file is %s and new file is %s"
             old-location
             new-location)
    (write-file new-location t)
    (when (and old-location
               (file-exists-p new-location)
               (not (string-equal old-location new-location)))
      (delete-file old-location))))

(add-to-list 'load-path "~/github/faker")
(setq faker-directory "/Users/blue0513/.emacs.d/faker/")
(require 'faker)

;; shackle
(require 'shackle)
(setq shackle-rules
      '((magit-stashes-mode :align below :ratio 0.3)
 	(yari-mode :align below :ratio 0.3)
 	(compilation-mode :align below :ratio 0.3)
        (rspec-compilation-mode :align below :ratio 0.2 :noselect t)
        (inf-ruby-mode :align below :ratio 0.2 :noselect t)
        ))
(shackle-mode 1)
(setq shackle-lighter "")

(add-hook 'ruby-mode-hook 'eldoc-mode)

;; spec-jump
(add-to-list 'load-path "~/github/spec-jump")
(require 'spec-jump)
(global-set-key (kbd "H-s") 'spec-jump)

;; rspec-mode
(eval-after-load 'rspec-mode
  '(rspec-install-snippets))
(setq compilation-scroll-output t)
(setenv "PATH" (concat (expand-file-name "~/.rbenv/shims:") (getenv "PATH")))
;; (defun fixed-rspec-compilation-window ()
;;   (setq window-size-fixed 'height))
(add-hook 'rspec-before-verification-hook (lambda() (instant-fixed-window-height)))
(add-hook 'inf-ruby-mode-hook (lambda() (instant-fixed-window-height)))

(add-to-list 'load-path "~/github/instant-fixed-window")
(require 'instant-fixed-window)

;; color-identifiers
(add-hook 'after-init-hook 'global-color-identifiers-mode)

;; emacs-git-messenger
(global-set-key (kbd "H-u") 'git-messenger:popup-message)

;; sqlint
(eval-after-load 'flycheck
  '(progn
     (flycheck-add-mode 'sql-sqlint 'sql-mode)))

;; https://sam217pa.github.io/2016/09/01/emacs-iterm-integration/
(defun iterm-focus ()
  (interactive)
  (do-applescript
   " do shell script \"open -a iTerm\"\n"
   ))

(defun get-file-dir-or-home ()
  "If inside a file buffer, return the directory, else return home"
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
	"~/"
      (file-name-directory filename))))

(defun iterm-goto-filedir-or-home ()
  "Go to present working dir and focus iterm"
  (interactive)
  (do-applescript
   (concat
    " tell application \"iTerm2\"\n"
    "   tell the current session of current window\n"
    (format "     write text \"cd %s\" \n" (get-file-dir-or-home))
    "   end tell\n"
    " end tell\n"
    " do shell script \"open -a iTerm\"\n"
    ))
  )

(add-to-list 'load-path "~/github/rubocop-fix-file.el")
(require 'rubocop-fix-file)
(global-set-key (kbd "H-r") 'rubocop-fix-file)

;; prescient.el
;; (ivy-prescient-mode t)
(company-prescient-mode t)
(prescient-persist-mode t)

;; gited
(require 'gited)

;; web-search
(global-set-key (kbd "H-w") 'web-search)

;; Clear the eshell buffer.
;; https://emacs.stackexchange.com/a/35806
(defun eshell-clear()
  (interactive)
  (let ((eshell-buffer-maximum-lines 0))
    (eshell-truncate-buffer)))

(add-hook 'eshell-mode-hook
          (lambda ()
            (define-key eshell-mode-map [(super hyper r)] #'eshell-clear)))

(add-to-list 'load-path "~/github/copy-current-git-path")
(require 'copy-current-git-path)

;; (require 'markdown-preview-mode)
;; (setq markdown-preview-stylesheets (list "https://cdn.jsdelivr.net/npm/github-markdown-css/github-markdown.min.css"))
(add-hook 'markdown-mode-hook
	  '(lambda()
	     (setq tab-width 2)))
;; markdown-mode
;; https://qiita.com/ybiquitous/items/34a4c3cb33f26136f670
(setq
 markdown-command "github-markup"
 markdown-command-needs-filename t
 markdown-content-type "application/xhtml+xml"
 markdown-css-paths '("https://cdn.jsdelivr.net/npm/github-markdown-css/github-markdown.min.css"
                      "http://cdn.jsdelivr.net/gh/highlightjs/cdn-release/build/styles/github.min.css")
 markdown-xhtml-header-content "
<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
<style>
body {
  box-sizing: border-box;
  max-width: 740px;
  width: 100%;
  margin: 40px auto;
  padding: 0 10px;
}
</style>
<script src='http://cdn.jsdelivr.net/gh/highlightjs/cdn-release/build/highlight.min.js'></script>
<script>
document.addEventListener('DOMContentLoaded', () => {
  document.body.classList.add('markdown-body');
  document.querySelectorAll('pre[lang] > code').forEach((code) => {
    code.classList.add(code.parentElement.lang);
    hljs.highlightBlock(code);
  });
});
</script>
")

(setq markdown-fontify-code-blocks-natively t)

(require 'bm)
(setq bm-in-lifo-order t)
(setq bm-cycle-all-buffers t)
(global-unset-key (kbd "M-b"))
(global-set-key (kbd "M-b") 'bm-toggle)
(global-set-key (kbd "H-b") 'bm-show-all)

;; マークのセーブ
(setq-default bm-buffer-persistence t)
;; セーブファイル名の設定
(setq bm-repository-file "~/.emacs.d/.bm-repository")
;; 起動時に設定のロード
(setq bm-restore-repository-on-load t)
(add-hook 'after-init-hook 'bm-repository-load)
(add-hook 'find-file-hooks 'bm-buffer-restore)
(add-hook 'after-revert-hook 'bm-buffer-restore)
;; 設定ファイルのセーブ
(add-hook 'kill-buffer-hook 'bm-buffer-save)
(add-hook 'auto-save-hook 'bm-buffer-save)
(add-hook 'after-save-hook 'bm-buffer-save)
(add-hook 'vc-before-checkin-hook 'bm-buffer-save)
;; Saving the repository to file when on exit
;; kill-buffer-hook is not called when emacs is killed, so we
;; must save all bookmarks first
(add-hook 'kill-emacs-hook '(lambda nil
                              (bm-buffer-save-all)
                              (bm-repository-save)))

(defun bm-counsel-get-list (bookmark-overlays)
  (-map (lambda (bm)
          (with-current-buffer (overlay-buffer bm)
            (let* ((line (replace-regexp-in-string
			  "\n$" ""
			  (buffer-substring (overlay-start bm)
					    (overlay-end bm))))
                   ;; line numbers start on 1
                   (line-num (+ 1 (count-lines (point-min) (overlay-start bm))))
                   (name (format "%s:%d: %s" (buffer-name) line-num line)))
	      `(,name . ,bm)
              )
            )
	  )
	bookmark-overlays))

(defun bm-counsel-find-bookmark ()
  (interactive)
  (let* ((bm-list (bm-counsel-get-list (bm-overlays-lifo-order t)))
         (bm-hash-table (make-hash-table :test 'equal))
         (search-list (-map (lambda (bm) (concat (car bm))) bm-list)))
    (-each bm-list (lambda (bm)
                     (puthash (car bm) (cdr bm) bm-hash-table)))
    (ivy-read "Find bookmark: "
              search-list
              :require-match t
              :keymap counsel-describe-map
              :action (lambda (chosen)
                        (let ((bookmark (gethash chosen bm-hash-table)))
                          (switch-to-buffer (overlay-buffer bookmark))
                          (bm-goto bookmark)))
              :sort t
              )))

;; (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
;; (setq highlight-indent-guides-method 'character)

;; (require 'indent-guide)
;; (indent-guide-global-mode)
;; (setq indent-guide-recursive t)

;; all-the-icon
(require 'all-the-icons)
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
(setq all-the-icons-ivy-file-commands
      '(counsel-find-file
	counsel-file-jump
	counsel-recentf
	counsel-projectile-find-file
	counsel-projectile-find-dir
	;; counsel-rg
	;; counsel-git-grep
	))

(all-the-icons-ivy-setup)

(add-to-list 'default-frame-alist '(cursor-type . bar))

;; (add-to-list 'load-path "~/github/right-click-context")
;; (require 'right-click-context)
;; (right-click-context-mode 1)
;; (define-key right-click-context-mode-map (kbd "C-c :") 'right-click-context-menu)

;; fill-column-indicator
;; (add-to-list 'load-path "~/github/Fill-Column-Indicator")
;; (require 'fill-column-indicator)
;; (add-hook 'prog-mode-hook 'fci-mode)
;; (add-hook 'ruby-mode-hook
;; 	  (lambda () (setq fci-rule-column 80)))

;; https://github.com/alpaker/Fill-Column-Indicator/issues/54
;; (defun on-off-fci-before-company(command)
;;   (when (string= "show" command)
;;     (turn-off-fci-mode))
;;   (when (string= "hide" command)
;;     (turn-on-fci-mode)))
;; (advice-add 'company-call-frontends :before #'on-off-fci-before-company)

;; (require 'ivy-explorer)
;; (ivy-explorer-mode 1)

(add-to-list 'load-path "~/github/cycle-frame-transparency")
(require 'cycle-frame-transparency)
(setq cft--trasparent 80)
(global-set-key (kbd "H-p") 'cycle-transparency)

(add-to-list 'load-path "~/github/git-counsel-recentf")
(require 'git-counsel-recentf)
(global-set-key [(super shift r)] 'git-counsel-recentf)

;; show trailing whitesppace
(add-to-list 'load-path "~/dotfiles/.emacs.d/highlight-chars")
(require 'highlight-chars)
(add-hook 'prog-mode-hook
	  (lambda () (hc-highlight-trailing-whitespace)))

(add-to-list 'load-path "~/github/flycheck-flash-mode-line")
(require 'flycheck-flash-mode-line)
(flycheck-flash-mode-line t)

;; yequake
(add-to-list 'load-path "~/github/yequake")
(require 'yequake)
(setq yequake-frames
      '(("scratch" .
         ((name . "scratch")
          (width . 0.75)
          (height . 0.35)
          (alpha . 0.80)
          (buffer-fns . ("*scratch*"))
          (frame-parameters . ((undecorated . t)))))))

;; textlint with flycheck
(flycheck-define-checker textlint
  "A linter for textlint."
  :command ("npx" "textlint"
            "--format" "unix"
            "--plugin"
            (eval
             (if (string= "tex" (file-name-extension buffer-file-name))
                 "latex"
               "@textlint/text"))
            source-inplace)
  :error-patterns
  ((warning line-start (file-name) ":" line ":" column ": "
            (message (one-or-more not-newline)
                     (zero-or-more "\n" (any " ") (one-or-more not-newline)))
            line-end))
  :modes (text-mode latex-mode org-mode markdown-mode))
(add-to-list 'flycheck-checkers 'textlint)

;; (require 'lsp-mode)
;; (add-hook 'ruby-mode-hook #'lsp)

;; (require 'company-lsp)
;; (push 'company-lsp company-backends)

;; (require 'lsp-ui)
;; (add-hook 'lsp-mode-hook 'lsp-ui-mode)

(require 'eglot)
(add-hook 'ruby-mode-hook 'eglot-ensure)
(define-key eglot-mode-map (kbd "s-f") 'xref-find-references)
;; (add-to-list 'eglot-server-programs '(ruby-mode . ("solargraph" "socket --port 49344")))
;; (add-to-list 'eglot-server-programs '(scala-mode . ("metals-emacs")))
(setq eglot-sync-connect nil)
(setq eglot-connect-timeout nil)
(setq eglot-ignored-server-capabilites
      '(:hoverProvider :signatureHelpProvider))

(require 'ivy-xref)
(setq xref-show-xrefs-function #'ivy-xref-show-xrefs)

;; (require 'historian)
;; (historian-mode 1)
;; (ivy-historian-mode 1)

(add-to-list 'load-path "~/github/auto-fix")
(require 'auto-fix)
(defun setup-ruby-auto-fix ()
  (setq-local auto-fix-command "rubocop")
  (setq-local auto-fix-option "-a"))

(add-hook 'ruby-mode-hook #'setup-ruby-auto-fix)

(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)

;;; https://blog.shibayu36.org/entry/2017/08/07/190421
;;; 現在のファイルをIntelliJで開く
(defun open-by-intellij ()
  (interactive)
  (shell-command "~/Desktop/test.sh"))

;; wgrep
;;; eでwgrepモードにする
(setq wgrep-enable-key "e")
;;; wgrep終了時にバッファを保存
(setq wgrep-auto-save-buffer t)
;;; read-only bufferにも変更を適用する
(setq wgrep-change-readonly-file t)

(add-to-list 'load-path "~/github/buffer-expose")
(require 'buffer-expose)
(setq buffer-expose-max-num-windows 8)
(setq buffer-expose-max-num-buffers 100)
(setq buffer-expose-hide-headerlines t)
(setq buffer-expose-hide-modelines t)
(setq buffer-expose-rescale-factor 0.9)
(setq buffer-expose-hide-regexes
      '("init.el" ".*log.*" "*Messages*" "*Help*" "*Compile-Log*" "*scratch*"))

;; (require 'alert)
;; (alert "hogehoge" :style 'osx-notifier)
;; (alert "hogehoge")

(add-to-list 'load-path "~/github/phantom-inline-comment")
(require 'phantom-inline-comment)
(global-unset-key (kbd "M-p"))
(global-set-key (kbd "M-p") 'phantom-inline-comment)
(global-unset-key (kbd "M-d"))
(global-set-key (kbd "M-d") 'phantom-inline-comment-delete)
(phantom-inline-comment-auto-save-mode t)
(add-hook 'prog-mode-hook 'phantom-inline-comment-auto-restore-mode)

;; second-sight
(add-to-list 'load-path "~/github/second-sight")
(require 'second-sight)
(global-set-key (kbd "H-y") 'second-sight)

;; zenrei
(add-to-list 'load-path "~/github/zenrei.el")
(require 'zenrei)

;; point-history
(add-to-list 'load-path "~/github/point-history")
;; (add-to-list 'load-path "~/Desktop/point-history")
(require 'point-history)
(point-history-mode t)
;; (global-set-key (kbd "M-l") 'point-history-show)

(add-to-list 'load-path "~/github/ivy-point-history")
(require 'ivy-point-history)
(global-set-key (kbd "M-l") 'ivy-point-history)

(require 'ivy-posframe)
(setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-center)))
(ivy-posframe-mode t)
(setq ivy-posframe-height 40)
(setq ivy-posframe-parameters
      '((left-fringe . 8)
        (right-fringe . 8)))

(add-to-list 'load-path "~/github/fixed-window")
(require 'fixed-window)
(setq fixed-window-width-ratio 0.8)
(setq fixed-window-disable-switch nil)
(setq fixed-window-dedicated nil)

(require 'company-box)
;; (add-hook 'company-mode-hook 'company-box-mode)
(setq company-box-icons-alist 'company-box-icons-all-the-icons)
(setq company-box-show-single-candidate t)
(setq company-box-max-candidates 50)
(with-eval-after-load 'all-the-icons
  (declare-function all-the-icons-faicon 'all-the-icons)
  (declare-function all-the-icons-material 'all-the-icons)
  (setq company-box-icons-all-the-icons
        `((Unknown . ,(all-the-icons-material "find_in_page" :height 0.9 :v-adjust -0.2))
          (Text . ,(all-the-icons-material "text_fields" :height 0.9 :v-adjust -0.2))
          (Method . ,(all-the-icons-faicon "cube" :height 0.9 :v-adjust -0.06 :face 'all-the-icons-purple))
          (Function . ,(all-the-icons-faicon "cube" :height 0.9 :v-adjust -0.06 :face 'all-the-icons-purple))
          (Constructor . ,(all-the-icons-faicon "cube" :height 0.9 :v-adjust -0.06 :face 'all-the-icons-purple))
          (Field . ,(all-the-icons-faicon "tag" :height 0.9 :v-adjust -0.06 :face 'all-the-icons-blue))
          (Variable . ,(all-the-icons-faicon "tag" :height 0.9 :v-adjust -0.06 :face 'all-the-icons-blue))
          (Class . ,(all-the-icons-material "settings_input_component" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-orange))
          (Interface . ,(all-the-icons-material "share" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-blue))
          (Module . ,(all-the-icons-material "view_module" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-blue))
          (Property . ,(all-the-icons-faicon "wrench" :height 0.9 :v-adjust -0.06))
          (Unit . ,(all-the-icons-material "settings_system_daydream" :height 0.9 :v-adjust -0.2))
          (Value . ,(all-the-icons-material "format_align_right" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-blue))
          (Enum . ,(all-the-icons-material "storage" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-orange))
          (Keyword . ,(all-the-icons-material "filter_center_focus" :height 0.9 :v-adjust -0.2))
          (Snippet . ,(all-the-icons-material "format_align_center" :height 0.9 :v-adjust -0.2))
          (Color . ,(all-the-icons-material "palette" :height 0.9 :v-adjust -0.2))
          (File . ,(all-the-icons-faicon "file-o" :height 0.9 :v-adjust -0.06))
          (Reference . ,(all-the-icons-material "collections_bookmark" :height 0.9 :v-adjust -0.2))
          (Folder . ,(all-the-icons-faicon "folder-open" :height 0.9 :v-adjust -0.06))
          (EnumMember . ,(all-the-icons-material "format_align_right" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-blueb))
          (Constant . ,(all-the-icons-faicon "square-o" :height 0.9 :v-adjust -0.06))
          (Struct . ,(all-the-icons-material "settings_input_component" :height 0.9 :v-adjust -0.2 :face 'all-the-icons-orange))
          (Event . ,(all-the-icons-faicon "bolt" :height 0.9 :v-adjust -0.06 :face 'all-the-icons-orange))
          (Operator . ,(all-the-icons-material "control_point" :height 0.9 :v-adjust -0.2))
          (TypeParameter . ,(all-the-icons-faicon "arrows" :height 0.9 :v-adjust -0.06))
          (Template . ,(all-the-icons-material "format_align_center" :height 0.9 :v-adjust -0.2)))))

;; (require 'company-tabnine)
;; (push 'company-tabnine company-backends)

;; (require 'ruby-electric)
;; (add-hook 'ruby-mode-hook
;;           '(lambda () (ruby-electric-mode t)))

;; (require 'highlight-parentheses)
;; (add-hook 'prog-mode-hook 'highlight-parentheses-mode)

;; (add-to-list 'load-path "~/github/ruby-block")
;; (require 'ruby-block)
;; (ruby-block-mode t)
;; (setq ruby-block-highlight-toggle t)

;; (add-to-list 'load-path "~/github/ruby-block-posframe")
;; (require 'ruby-block-posframe)

;; (add-to-list 'load-path "~/tidal")
;; (require 'haskell-mode)
;; (require 'tidal)

;; js-comint as REPL
(require 'js-comint)

;; auto-package-update
(auto-package-update-maybe)
(setq auto-package-update-interval 3) ;; 3 days
(auto-package-update-at-time "03:00") ;; 3 am
(add-hook 'auto-package-update-before-hook
          (lambda () (message "I will update packages now")))
(add-hook 'auto-package-update-after-hook
          (lambda () (message "Already updated packages !")))
(setq auto-package-update-delete-old-versions t)
;; (auto-package-update-now)

;; custom-set-faces
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(avy-goto-char-timer-face ((t :background "DeepSkyBlue4")))
 '(avy-lead-face ((t :background "white")))
 '(company-preview-search ((t nil)))
 '(company-tooltip ((t (:background "#3C3D37" :foreground "gray82"))))
 '(company-tooltip-search ((t (:background "gray100" :foreground "PaleGreen4"))))
 '(company-tooltip-search-selection ((t (:background "PaleGreen4" :foreground "gray100"))))
 '(ediff-even-diff-A ((t (:background "dark red" :foreground "white smoke"))))
 '(ediff-even-diff-B ((t (:background "dark green" :foreground "white smoke"))))
 '(ediff-odd-diff-A ((t (:background "dark red" :foreground "white smoke"))))
 '(ediff-odd-diff-B ((t (:background "dark green" :foreground "white"))))
 '(elscreen-tab-background-face ((t (:background "black"))))
 '(elscreen-tab-control-face ((t (:background "black" :foreground "gray60" :underline "Gray50"))))
 '(elscreen-tab-current-screen-face ((t (:background "black" :foreground "dark magenta"))))
 '(elscreen-tab-other-screen-face ((t (:background "black" :foreground "gray60"))))
 '(flycheck-posframe-background-face ((t (:background "MediumPurple4" :foreground "ghost white"))))
 '(flyspell-duplicate ((t (:underline "dark gray"))))
 '(flyspell-incorrect ((t (:underline "dark gray"))))
 '(fold-this-overlay ((t (:inherit default :background "dim gray"))))
 '(font-latex-sectioning-2-face ((t (:inherit font-latex-sectioning-3-face :foreground "cyan1" :height 1.1))))
 '(font-latex-sectioning-3-face ((t (:inherit font-latex-sectioning-4-face :foreground "gold" :height 1.1))))
 '(font-latex-sectioning-4-face ((t (:inherit font-latex-sectioning-5-face :foreground "SeaGreen3" :height 1.1))))
 '(font-lpatex-sectioning-1-face ((t (:inherit font-latex-sectioning-2-face :foreground "orange" :height 1.1))))
 '(hc-trailing-whitespace ((t (:underline (:color "dark red" :style wave)))))
 '(highlight-symbol-face ((t (:background "dark magenta"))))
 '(highlight-unique-symbol:face ((t :background "DarkSlateGrey")))
 '(ivy-posframe-cursor ((t (:inherit default :background "dark magenta"))))
 '(markdown-header-face-1 ((t (:inherit org-level-1))))
 '(markdown-header-face-2 ((t (:inherit org-level-2))))
 '(markdown-header-face-3 ((t (:inherit org-level-3))))
 '(markdown-header-face-4 ((t (:inherit org-level-4))))
 '(markdown-header-face-5 ((t (:inherit org-level-5))))
 '(markdown-header-face-6 ((t (:inherit org-level-6))))
 '(pomodoro:work-face ((t (:foreground "blue1"))))
 '(popup-isearch-match ((t (:background "SpringGreen4" :foreground "gray78"))))
 '(popup-menu-face ((t (:background "gray25" :foreground "dark gray"))))
 '(popup-menu-selection-face ((t (:background "DeepSkyBlue4" :foreground "gray82"))))
 '(popup-tip-face ((t (:background "MediumPurple4" :foreground "#F8F8F2"))))
 '(symbol-overlay-default-face ((t (:background "dark magenta"))))
 '(web-mode-comment-face ((t (:foreground "#587F35"))))
 '(web-mode-css-at-rule-face ((t (:foreground "#DFCF44"))))
 '(web-mode-css-property-name-face ((t (:foreground "#87CEEB"))))
 '(web-mode-css-pseudo-class ((t (:foreground "#DFCF44"))))
 '(web-mode-css-selector-face ((t (:foreground "#DFCF44"))))
 '(web-mode-css-string-face ((t (:foreground "#D78181"))))
 '(web-mode-doctype-face ((t (:foreground "#4A8ACA"))))
 '(web-mode-html-attr-equal-face ((t (:foreground "#FFFFFF"))))
 '(web-mode-html-attr-name-face ((t (:foreground "#87CEEB"))))
 '(web-mode-html-attr-value-face ((t (:foreground "#D78181"))))
 '(web-mode-html-tag-face ((t (:foreground "#4A8ACA"))))
 '(web-mode-server-comment-face ((t (:foreground "#587F35"))))
 '(whitespace-trailing ((t (:inverse-video t)))))

;; custom-set-variables


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun display-startup-echo-area-message ()
  (message "Let the hacking begin!"))

;;; end;;;
(put 'erase-buffer 'disabled nil)
