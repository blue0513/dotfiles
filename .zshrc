
# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Reset color when split pane
echo -n -e "\033]50;SetProfile=Default\a"

# Customize to your needs...
export GREP_COLOR='31'
# export GREP_COLOR='37;45'

#vagrant
export VAGRANT_DEFAULT_PROVIDER=vmware_fusion

#python
export PATH="$HOME/.pyenv/shims:$PATH"
# export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/site-packages/

#emacs
alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs -nw'
alias e='emacsclient'

#latex
export LATEXPATH=$LATEXPATH:/opt/local/bin/pdflatex

# ssh-add
ssh-add -A 2>/dev/null;

# GO path
if [ -x "`which go`" ]; then
   export GOPATH=$HOME/Go
   export PATH=$PATH:$GOPATH/bin
fi

peco-select-history() {
    BUFFER=$(history 1 | sort -k1,1nr | perl -ne 'BEGIN { my @lines = (); } s/^\s*\d+\*?\s*//; $in=$_; if (!(grep {$in eq $_} @lines)) { push(@lines, $in); print $in; }' | peco --query "$LBUFFER")
        CURSOR=${#BUFFER}
	    zle reset-prompt
	    
}
zle -N peco-select-history
bindkey '^R' peco-select-history

ZSH_HIGHLIGHT_STYLES[path]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[path_prefix]='none'

#cdとlsの省略
setopt auto_cd
function chpwd() { ls }

google() {
    local str opt
    if [ $# != 0 ]; then
        for i in $*; do
            # $strが空じゃない場合、検索ワードを+記号でつなぐ(and検索)
            str="$str${str:++}$i"
        done
        opt='search?num=100'
        opt="${opt}&q=${str}"
    fi
    open -a Google\ Chrome http://www.google.co.jp/$opt
}

qiita() {
    local str opt
    if [ $# != 0 ]; then
        for i in $*; do
            # $strが空じゃない場合、検索ワードを+記号でつなぐ(and検索)
            str="$str${str:++}$i"
        done
        opt='search?num=100'
        opt="${opt}&q=${str}"
    fi
    open -a Google\ Chrome http://qiita.com/$opt
}

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
# setopt hist_ignore_dups     # ignore duplication command history list
setopt share_history        # share command history data

# Ctrl+Dでログアウトしてしまうことを防ぐ
setopt IGNOREEOF

# git stashで `stash@0 is not a valid reference` を防ぐ
unsetopt BRACE_CCL

# enhancd
source "$HOME/.zsh.d/enhancd/init.sh"
ENHANCD_FILTER=peco:fzf

# zsh-notify
source "$HOME/.zsh.d/zsh-notify/notify.plugin.zsh"

# z
source "$HOME/.zsh.d/z/z.sh"

# mysql-colorize
source "$HOME/.zsh.d/mysql-colorize/mysql-colorize.plugin.zsh"

function peco-z-search
{
  which peco z > /dev/null
  if [ $? -ne 0 ]; then
    echo "Please install peco and z"
    return 1
  fi
  local res=$(z | sort -rn | cut -c 12- | peco)
  if [ -n "$res" ]; then
    BUFFER+="cd $res"
    zle accept-line
  else
    return 1
  fi
}
zle -N peco-z-search
bindkey '^o' peco-z-search

# zsh-autosuggestions
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=240'

# zsh-completions
if [ -e /usr/local/share/zsh-completions ]; then
    fpath=(/usr/local/share/zsh-completions $fpath)
fi

# 補完で小文字でも大文字にマッチさせる
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# beep を無効にする
setopt no_beep

# command line editor
export EDITOR='emacs -nw'
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey '^Xe' edit-command-line

# # 以降をコメントとして扱う
setopt interactive_comments

# 終了ステータスが0以外の場合にステータスを表示する
setopt print_exit_value

# zsh-syntax-highlighting-filetypes
#source "$HOME/.zsh.d/zsh-syntax-highlighting-filetypes/zsh-syntax-highlighting-filetypes.zsh"

# Enterを押すと，lsとgit statusを表示
function do_enter() {
    if [ -n "$BUFFER" ]; then
        zle accept-line
        return 0
    fi
    echo
    ls
    # ↓おすすめ
    # ls_abbrev
    if [ "$(git rev-parse --is-inside-work-tree 2> /dev/null)" = 'true' ]; then
        echo
        echo -e "\e[0;33m--- git status ---\e[0m"
        git status -sb
    fi
    zle reset-prompt
    return 0
}
zle -N do_enter
bindkey '^m' do_enter

# zsh-history-substring-search
source "/usr/local/share/zsh-history-substring-search/zsh-history-substring-search.zsh"
bindkey -M emacs '^P' history-substring-search-up
bindkey -M emacs '^N' history-substring-search-down

# 実行結果をpecoに送る
function peco-buffer() {
    BUFFER=$(eval ${BUFFER} | peco)
    CURSOR=0
}
zle -N peco-buffer
bindkey "^[p" peco-buffer

# zsh-autopair
source "$HOME/.zsh.d/zsh-autopair/autopair.zsh"

# zsh-gomi
source "$HOME/.zsh.d/zsh-gomi/gomi.zsh"

# zsh-reminder
setopt clobber # touchによるリダイレクト上書きを許可
source "$HOME/.zsh.d/oh-my-zsh-reminder/reminder.plugin.zsh"

# zsh-git-escape-magic
source "$HOME/.zsh.d/zsh-git-escape-magic/git-escape-magic"

# alias
alias -g mf='mdfind'
alias -g gs='git status'
alias -g gb='git branch'
alias -g ga='git add'
alias -g gc='git commit -m'
alias -g mv='mv -v -i'
alias -g ec='emacsclient -n'
alias -g glog='git log --oneline --decorate --graph --branches --tags --remotes --color'
alias -g grep='grep -n '
alias -g rg='rg -n '
alias -g lsp='lsp -p '
alias alert="osascript -e 'display notification \"get work!\" with title \"Command Finish!\"' -e 'beep 2'"
alias -g A="&& osascript -e 'display notification \" get work!\" with title \"Command Finish!\"' -e 'beep 2'"
alias -g findtext='my_find_text'
alias -g gch='git checkout `git branch | peco | sed -e "s/\* //g" | awk "{print \$1}"`'
alias -g oc='open .'
alias -g cat='ccat' # brew install ccat
alias -g gsh='my_git_stash_save'

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# for pdf-tools in emacs
export PKG_CONFIG_PATH=/usr/local/Cellar/zlib/1.2.8/lib/pkgconfig:/usr/local/lib/pkgconfig:/opt/X11/lib/pkgconfig

export VISUAL='emacs'

#####################
# My custom functions
#####################

# git stash with message (force) 
function my_git_stash_save() {
    if [ "$1" = "" ]; then
	echo "You must set the message!";
	echo "gsh [Your_Message]";
    else
	git stash save ${@:1};
    fi
}

# find text
function my_find_text() {
    target_dir=$1
    target_string=$2
    find ${target_dir} -type f -print | xargs grep ${target_string}
}

function ssh_color() {
    # imgcat ~/Desktop/zoi_70.jpg
    # figlet -f slant DEPLOY SERVER
    
 case $1 in
     aoki* ) echo -e "\033]50;SetProfile=TaijuServer\a"
	     figlet -f slant DEPLOY SERVER;;
   *) echo -e "\033]50;SetProfile=Default\a" ;;
 esac
  ssh $@
  echo -e "\033]50;SetProfile=Default\a"
}

# stash IDを取得
function peco-git-stash () {
  local current_buffer=$BUFFER
  local stash="$(git stash list | peco | awk -F'[ :]' '{print $1}')"
  BUFFER="${current_buffer}${stash}"
  # カーソル位置を末尾に移動
  CURSOR=$#BUFFER
}
zle -N peco-git-stash
bindkey '^gs' peco-git-stash

alias ssh='ssh_color'
compdef _ssh ssh_color=ssh

export PATH="$HOME/Desktop/pull-req:$PATH"
export PATH="$HOME/shellscript/iterm_control:$PATH"

function view-large-log-file () {
    number=$1
    file=$2
    tail -n ${number} ${file}
}

function view-large-log-file-with-grep () {
    number=$1
    file=$2
    string=$3
    view-large-log-file ${number} ${file} | grep "${string}"
}

alias lcat='view-large-log-file'
alias lcg='view-large-log-file-with-grep'

export PATH="$HOME/shellscript/reload_chrome:$PATH"
export PATH="$HOME/shellscript/switch_tab_chrome:$PATH"
export PATH="$HOME/shellscript/scroll_chrome:$PATH"

export LESSOPEN="| /usr/local/Cellar/source-highlight/3.1.8_9/bin/src-hilite-lesspipe.sh %s"
export LESS=" -R "

# alias hilite='/usr/local/Cellar/source-highlight/3.1.8_9/bin/src-hilite-lesspipe.sh'
alias less='less -N'
