#!/bin/bash

# zshに切り替え
echo "switch to zsh"
_PID=$$;
_PPID=$(ps -o ppid -p $_PID | tail -n 1);

if ps -p $_PPID | grep -qs bash ; then
    $zsh
fi

# 環境変数
echo "set env"
setopt EXTENDED_GLOB

# submodule
echo "set submodule"
git submodule init
git submodule update

# そのままだとfailするため，実行
echo "set submodule again"
cd ~/dotfiles/.zprezto/prezto/modules/autosuggestions/external/
git submodule init
git submodule update

# シンボリックリンクの削除・作成
echo "set symbolic link"
cd ~/

ZSH_FILES=(.zlogin .zlogout .zprofile .zshenv .zpreztorc)
for file in ${ZSH_FILES[@]}
do
    echo $file
    if [ -e $file ]; then
	rm $file
    fi
done

ln -s ~/dotfiles/.zprezto/runcoms/zlogin .zlogin
ln -s ~/dotfiles/.zprezto/runcoms/zlogout .zlogout
ln -s ~/dotfiles/.zprezto/runcoms/zpreztorc .zpreztorc
ln -s ~/dotfiles/.zprezto/runcoms/zprofile .zprofile
ln -s ~/dotfiles/.zprezto/runcoms/zshenv .zshenv

# dotfilesのシンボリックリンク削除・作成
echo "set symbolic link of dotfiles"
DOT_FILES=(.emacs.d .peco .pryrc .zprezto .zsh.d .zshrc)

for file in ${DOT_FILES[@]}
do
    echo $file
    if [ -e $file ]; then
	rm $file
    fi

    ln -s $HOME/dotfiles/$file
done

# 再読込
echo "exec zsh"
exec zsh
